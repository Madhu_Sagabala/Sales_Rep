import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:sales_rep/Go_Route.dart';
import 'package:sales_rep/flutter_flow/flutter_flow_theme.dart';
import 'package:salomon_bottom_bar/salomon_bottom_bar.dart';

class BottomNavigation extends StatefulWidget {
  final int activeIndex;

  const BottomNavigation({
    super.key,
    required this.activeIndex,
  });

  @override
  State<BottomNavigation> createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation> {
  int currentindex = 0;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: SalomonBottomBar(
        itemShape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        selectedColorOpacity: 1,
        selectedItemColor: const Color(0xff005BFE),
        currentIndex: widget.activeIndex,
        backgroundColor:
            FlutterFlowTheme.of(context).bottombar.withOpacity(0.25),
        items: <SalomonBottomBarItem>[
          SalomonBottomBarItem(
            icon: widget.activeIndex == 0
                ? SvgPicture.asset(
                    'assets/images/HomeIocn.svg',
                    height: 24,
                    width: 24,
                    color: Colors.white,
                  )
                : SvgPicture.asset(
                    'assets/images/HomeIocn.svg',
                    height: 24,
                    width: 24,
                    color: FlutterFlowTheme.of(context).text,
                  ),
            title: const Text("Home",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Urbanist',
                    fontSize: 12)),
          ),
          SalomonBottomBarItem(
            icon: widget.activeIndex == 1
                ? SvgPicture.asset(
                    'assets/images/VisitIon.svg',
                    height: 24,
                    width: 24,
                    color: Colors.white,
                  )
                : SvgPicture.asset(
                    'assets/images/VisitIon.svg',
                    height: 24,
                    width: 24,
                    color: FlutterFlowTheme.of(context).text,
                  ),
            title: const Text("Visits",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Urbanist',
                    fontSize: 12)),
          ),
          SalomonBottomBarItem(
            icon: widget.activeIndex == 2
                ? SvgPicture.asset(
                    'assets/images/LocationIcon.svg',
                    height: 24,
                    width: 24,
                    color: Colors.white,
                  )
                : SvgPicture.asset(
                    'assets/images/LocationIcon.svg',
                    height: 24,
                    width: 24,
                    color: FlutterFlowTheme.of(context).text,
                  ),
            title: const Text("Location",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Urbanist',
                    fontSize: 12)),
          ),
          SalomonBottomBarItem(
            icon: widget.activeIndex == 3
                ? SvgPicture.asset('assets/images/GalleryIcon.svg',
                    height: 24, width: 24, color: Colors.white)
                : SvgPicture.asset(
                    'assets/images/GalleryIcon.svg',
                    height: 24,
                    width: 24,
                    color: FlutterFlowTheme.of(context).text,
                  ),
            title: const Text("Gallery",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Urbanist',
                    fontSize: 12)),
          ),
          SalomonBottomBarItem(
            icon: widget.activeIndex == 4
                ? SvgPicture.asset(
                    'assets/images/ProfileIcon.svg',
                    height: 24,
                    width: 24,
                    color: Colors.white,
                  )
                : SvgPicture.asset(
                    'assets/images/ProfileIcon.svg',
                    height: 24,
                    width: 24,
                    color: FlutterFlowTheme.of(context).text,
                  ),
            title: const Text("Profile",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Urbanist',
                    fontSize: 12)),
          ),
        ],
        onTap: (index) async {
          switch (index) {
            case 0:
              if (widget.activeIndex != 0) Get.toNamed(Routes.dashboard);

              break;
            case 1:
              if (widget.activeIndex != 1) Get.toNamed(Routes.totalVisits);

              break;
            case 2:
              if (widget.activeIndex != 2) Get.toNamed(Routes.totalLocatins);

              break;
            case 3:
              if (widget.activeIndex != 3) Get.toNamed(Routes.gallery);

              break;
            case 4:
              if (widget.activeIndex != 4) Get.toNamed(Routes.profile);
          }
        },
      ),
    );
  }
}
