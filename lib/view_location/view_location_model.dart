import '/flutter_flow/flutter_flow_util.dart';
import 'view_location_widget.dart' show ViewLocationWidget;
import 'package:flutter/material.dart';

class ViewLocationModel extends FlutterFlowModel<ViewLocationWidget> {
  ///  State fields for stateful widgets in this page.

  final unfocusNode = FocusNode();

  @override
  void initState(BuildContext context) {}

  @override
  void dispose() {
    unfocusNode.dispose();
  }
}
