import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

getsuccesssnackbar({String message = ''}) {
  Get.showSnackbar(GetSnackBar(
    borderRadius: 20,
    margin: const EdgeInsets.all(16),
    animationDuration: const Duration(seconds: 1),
    duration: const Duration(seconds: 3),
    icon: Padding(
      padding: const EdgeInsets.only(bottom: 10.0, right: 23),
      child: SvgPicture.asset(''),
    ),
    backgroundColor: const Color(0xFF4f76ff),
    snackPosition: SnackPosition.TOP,
    messageText: Container(
      padding: const EdgeInsets.only(right: 23),
      alignment: Alignment.center,
      child: Text(
        message,
        style: const TextStyle(
          fontWeight: FontWeight.w400,
          fontSize: 14,
          color: Colors.white,
        ),
      ),
    ),
  ));
}
