import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

geterrorsnackbar({String? message}) {
  Get.showSnackbar(GetSnackBar(
    borderRadius: 20,
    margin: const EdgeInsets.all(16),
    animationDuration: const Duration(seconds: 1),
    duration: const Duration(seconds: 2),
    icon: Padding(
      padding: const EdgeInsets.only(bottom: 10.0, right: 23),
      child: SvgPicture.asset('assets/images/Green Chilli.svg'),
    ),
    backgroundColor: const Color(0xFFDB2B3A),
    snackPosition: SnackPosition.TOP,
    messageText: Container(
      padding: const EdgeInsets.only(right: 23),
      alignment: Alignment.center,
      child: Text(
        '$message',
        style: const TextStyle(
          fontWeight: FontWeight.w400,
          fontSize: 14,
          color: Colors.white,
        ),
      ),
    ),
  ));
}
