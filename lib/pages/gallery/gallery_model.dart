import '/components/folder_card_widget.dart';
import '/components/galery_view_widget.dart';
import '/flutter_flow/flutter_flow_util.dart';
import 'gallery_widget.dart' show GalleryWidget;
import 'package:flutter/material.dart';

class GalleryModel extends FlutterFlowModel<GalleryWidget> {
  ///  State fields for stateful widgets in this page.

  final unfocusNode = FocusNode();
  // Model for FolderCard component.
  late FolderCardModel folderCardModel1;
  // Model for FolderCard component.
  late FolderCardModel folderCardModel2;
  // Model for FolderCard component.
  late FolderCardModel folderCardModel3;
  // Model for FolderCard component.
  late FolderCardModel folderCardModel4;
  // Model for FolderCard component.
  late FolderCardModel folderCardModel5;
  // Model for GaleryView component.
  late GaleryViewModel galeryViewModel1;
  // Model for GaleryView component.
  late GaleryViewModel galeryViewModel2;
  // Model for GaleryView component.
  late GaleryViewModel galeryViewModel3;
  // Model for GaleryView component.
  late GaleryViewModel galeryViewModel4;

  @override
  void initState(BuildContext context) {
    folderCardModel1 = createModel(context, () => FolderCardModel());
    folderCardModel2 = createModel(context, () => FolderCardModel());
    folderCardModel3 = createModel(context, () => FolderCardModel());
    folderCardModel4 = createModel(context, () => FolderCardModel());
    folderCardModel5 = createModel(context, () => FolderCardModel());
    galeryViewModel1 = createModel(context, () => GaleryViewModel());
    galeryViewModel2 = createModel(context, () => GaleryViewModel());
    galeryViewModel3 = createModel(context, () => GaleryViewModel());
    galeryViewModel4 = createModel(context, () => GaleryViewModel());
  }

  @override
  void dispose() {
    unfocusNode.dispose();
    folderCardModel1.dispose();
    folderCardModel2.dispose();
    folderCardModel3.dispose();
    folderCardModel4.dispose();
    folderCardModel5.dispose();
    galeryViewModel1.dispose();
    galeryViewModel2.dispose();
    galeryViewModel3.dispose();
    galeryViewModel4.dispose();
  }
}
