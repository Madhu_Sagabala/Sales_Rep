import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sales_rep/backend/custom_response.dart';
import 'package:sales_rep/backend/services/dashboard_stats_api.dart';
import 'package:sales_rep/backend/services/dashboard_total_visit_api.dart';
import 'package:sales_rep/models/dashboard_stats_model.dart';
import 'package:sales_rep/models/visite_list_model.dart';

class DashboardController extends GetxController {
  ///  State fields for stateful widgets in this page.

  final unfocusNode = FocusNode();
  // Model for visitsCard component.

  DashboardstatsResponse? statsresponsedata;
  VisitelistResponseDummy? visitelistResponse;
  var isloading = false.obs;
  Future<void> fetchstats() async {
    CustomResponse<DashboardstatsResponse> response =
        await TotalStatsApi().call();
    final data = response.data;
    statsresponsedata = data;
  }

  Future<void> fetchvisitlist() async {
    CustomResponse<VisitelistResponseDummy> response =
        await VisitslistApi().call();
    final data = response.data;
    visitelistResponse = data;
  }
}
