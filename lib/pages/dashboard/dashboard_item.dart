import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sales_rep/flutter_flow/flutter_flow_theme.dart';
import 'package:sales_rep/flutter_flow/flutter_flow_util.dart';

class DashboadrItem extends StatelessWidget {
  final String path;
  final String data;
  final String percent;
  final String text0;
  final Color cardColor;
  final Color textColor;
  const DashboadrItem(
      {super.key,
      required this.path,
      required this.data,
      required this.percent,
      required this.text0,
      required this.cardColor,
      required this.textColor});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: cardColor,
        borderRadius: BorderRadius.circular(12),
      ),
      child: Align(
        alignment: const AlignmentDirectional(0, 0),
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(8),
                    child: SvgPicture.asset(
                      path,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Text(
                    text0,
                    style: FlutterFlowTheme.of(context).labelSmall.override(
                          fontFamily: 'Urbanist',
                          color: textColor,
                          letterSpacing: 0,
                          fontWeight: FontWeight.w500,
                        ),
                  ),
                ].divide(const SizedBox(width: 8)),
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Padding(
                    padding: const EdgeInsetsDirectional.fromSTEB(0, 10, 0, 0),
                    child: Text(
                      data,
                      style:
                          FlutterFlowTheme.of(context).headlineMedium.override(
                                fontFamily: 'Urbanist',
                                color: textColor,
                                letterSpacing: 0,
                              ),
                    ),
                  ),
                ].divide(const SizedBox(width: 8)),
              ),
              SizedBox(
                width: 140,
                child: Divider(
                  thickness: 1,
                  color: FlutterFlowTheme.of(context).accent4,
                ),
              ),
              Padding(
                padding: const EdgeInsetsDirectional.fromSTEB(0, 2, 0, 0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    const Icon(
                      Icons.arrow_outward_sharp,
                      color: Colors.white,
                      size: 10,
                    ),
                    Text(
                      '$percent % vs last month',
                      style: FlutterFlowTheme.of(context).bodyMedium.override(
                            fontFamily: 'Urbanist',
                            color: textColor,
                            fontSize: 10,
                            letterSpacing: 0,
                          ),
                    ),
                  ].divide(const SizedBox(width: 2)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
