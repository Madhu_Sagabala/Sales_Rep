import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:sales_rep/bottom_navigation.dart';
import 'package:sales_rep/components/visits_card_widget_copy.dart';
import 'package:sales_rep/pages/dashboard/dashboard_item.dart';

import '/flutter_flow/flutter_flow_theme.dart';
import '/flutter_flow/flutter_flow_util.dart';
import 'dashboard_controller.dart';

export 'dashboard_controller.dart';

DashboardController dashboardController = Get.put(DashboardController());

class DashboardWidget extends StatefulWidget {
  const DashboardWidget({super.key});

  @override
  State<DashboardWidget> createState() => _DashboardWidgetState();
}

class _DashboardWidgetState extends State<DashboardWidget> {
  late Future<void> futuredata;
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    futuredata = fetchdata();
  }

  @override
  void dispose() {
    dashboardController.dispose();
    super.dispose();
  }

  Future<void> fetchdata() async {
    dashboardController.isloading.value = true;
    await Future.wait([
      dashboardController.fetchstats(),
      dashboardController.fetchvisitlist()
    ]);
    dashboardController.isloading.value = false;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: FutureBuilder(
          future: futuredata,
          builder: (context, snapshot) {
            return Scaffold(
              key: scaffoldKey,
              bottomNavigationBar: const BottomNavigation(activeIndex: 0),
              backgroundColor: FlutterFlowTheme.of(context).tertiary,
              body: dashboardController.isloading.value
                  ? const Center(child: CircularProgressIndicator())
                  : SafeArea(
                      top: true,
                      child: SingleChildScrollView(
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Padding(
                                    padding:
                                        const EdgeInsetsDirectional.fromSTEB(
                                            16.0, 0.0, 16.0, 0.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      children: [
                                        Row(
                                          mainAxisSize: MainAxisSize.max,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Row(
                                              mainAxisSize: MainAxisSize.max,
                                              children: [
                                                ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          0.0),
                                                  child: SvgPicture.asset(
                                                    'assets/images/Frame_12.svg',
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                                Text(
                                                  'Dashboard',
                                                  style: FlutterFlowTheme.of(
                                                          context)
                                                      .titleMedium
                                                      .override(
                                                        fontFamily: 'Urbanist',
                                                        color:
                                                            FlutterFlowTheme.of(
                                                                    context)
                                                                .text,
                                                        letterSpacing: 0.0,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                      ),
                                                ),
                                              ].divide(
                                                  const SizedBox(width: 11.0)),
                                            ),
                                            Row(
                                              mainAxisSize: MainAxisSize.max,
                                              children: [
                                                Container(
                                                  decoration:
                                                      const BoxDecoration(),
                                                  child: ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8.0),
                                                    child: SvgPicture.asset(
                                                      'assets/images/SearchIcon.svg',
                                                      fit: BoxFit.cover,
                                                      color:
                                                          FlutterFlowTheme.of(
                                                                  context)
                                                              .text,
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  decoration:
                                                      const BoxDecoration(),
                                                  child: ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8.0),
                                                    child: SvgPicture.asset(
                                                      'assets/images/NotificationBellIcon.svg',
                                                      color:
                                                          FlutterFlowTheme.of(
                                                                  context)
                                                              .text,
                                                      fit: BoxFit.cover,
                                                    ),
                                                  ),
                                                ),
                                              ].divide(
                                                  const SizedBox(width: 10.0)),
                                            ),
                                          ],
                                        ),
                                        Row(
                                          mainAxisSize: MainAxisSize.max,
                                          children: [
                                            Expanded(
                                              child: Container(
                                                decoration: BoxDecoration(
                                                  color: FlutterFlowTheme.of(
                                                          context)
                                                      .tertiary
                                                      .withOpacity(0.09),
                                                  boxShadow: const [
                                                    BoxShadow(
                                                      blurRadius: 12.0,
                                                      color: Color(0x17000000),
                                                      offset: Offset(0.0, 0.0),
                                                    )
                                                  ],
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          8.0),
                                                ),
                                                child: Padding(
                                                  padding: const EdgeInsets.all(
                                                      14.0),
                                                  child: Column(
                                                    mainAxisSize:
                                                        MainAxisSize.max,
                                                    children: [
                                                      Row(
                                                        mainAxisSize:
                                                            MainAxisSize.max,
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          Row(
                                                              mainAxisSize:
                                                                  MainAxisSize
                                                                      .max,
                                                              children: [
                                                                Container(
                                                                    width: 35.0,
                                                                    height:
                                                                        35.0,
                                                                    decoration:
                                                                        const BoxDecoration(
                                                                      color: Color(
                                                                          0x34005BFE),
                                                                      shape: BoxShape
                                                                          .circle,
                                                                    ),
                                                                    alignment:
                                                                        const AlignmentDirectional(
                                                                            0.0,
                                                                            0.0),
                                                                    child: const Padding(
                                                                        padding: EdgeInsetsDirectional.fromSTEB(
                                                                            5.0,
                                                                            5.0,
                                                                            5.0,
                                                                            5.0),
                                                                        child: FaIcon(
                                                                            FontAwesomeIcons
                                                                                .chartLine,
                                                                            color:
                                                                                Color(0xFF005BFE),
                                                                            size: 18.0))),
                                                                Text(
                                                                  'Performance',
                                                                  style: FlutterFlowTheme.of(
                                                                          context)
                                                                      .bodyLarge
                                                                      .override(
                                                                        fontFamily:
                                                                            'Urbanist',
                                                                        color: const Color(
                                                                            0xFF005BFE),
                                                                        letterSpacing:
                                                                            0.0,
                                                                        fontWeight:
                                                                            FontWeight.w500,
                                                                      ),
                                                                ),
                                                              ].divide(
                                                                  const SizedBox(
                                                                      width:
                                                                          12.0))),
                                                          Container(
                                                            decoration:
                                                                BoxDecoration(
                                                              color: const Color(
                                                                  0xFFC3CDFF),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          60.0),
                                                            ),
                                                            child: Padding(
                                                              padding:
                                                                  const EdgeInsetsDirectional
                                                                      .fromSTEB(
                                                                      14.0,
                                                                      7.0,
                                                                      14.0,
                                                                      7.0),
                                                              child: Row(
                                                                mainAxisSize:
                                                                    MainAxisSize
                                                                        .max,
                                                                children: [
                                                                  Text(
                                                                    'Jan - Jun',
                                                                    style: FlutterFlowTheme.of(
                                                                            context)
                                                                        .titleSmall
                                                                        .override(
                                                                          fontFamily:
                                                                              'Urbanist',
                                                                          color:
                                                                              Colors.black,
                                                                          letterSpacing:
                                                                              0.0,
                                                                        ),
                                                                  ),
                                                                  const Icon(
                                                                    Icons
                                                                        .keyboard_arrow_down,
                                                                    color: Colors
                                                                        .black,
                                                                    size: 18.0,
                                                                  ),
                                                                ].divide(
                                                                    const SizedBox(
                                                                        width:
                                                                            5.0)),
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      ClipRRect(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(0.0),
                                                        child: SvgPicture.asset(
                                                          'assets/images/Frame_35.svg',
                                                          fit: BoxFit.cover,
                                                        ),
                                                      ),
                                                    ].divide(const SizedBox(
                                                        height: 28.0)),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ].divide(const SizedBox(height: 20.0)),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsetsDirectional.fromSTEB(
                                  0, 16, 0, 0),
                              child: SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    DashboadrItem(
                                        cardColor: const Color(0xFF3640F0),
                                        path: 'assets/images/VisitsIcon.svg',
                                        data:
                                            '${dashboardController.statsresponsedata!.record!.totalVisits ?? ''}',
                                        textColor: Colors.white,
                                        percent: '29',
                                        text0: 'Total Visits'),
                                    const SizedBox(width: 16),
                                    DashboadrItem(
                                        cardColor: Color(0xFF64FFE3),
                                        path: 'assets/images/LocationIcon.svg',
                                        data:
                                            '${dashboardController.statsresponsedata!.record!.totalLocations ?? ''}',
                                        percent: '29',
                                        textColor: Colors.black,
                                        text0: 'Total Locations'),
                                    const SizedBox(width: 16),
                                    DashboadrItem(
                                        cardColor: Color(0xFF9769FF),
                                        path: 'assets/images/ImagesIcon.svg',
                                        data:
                                            '${dashboardController.statsresponsedata!.record!.totalImages ?? ''}',
                                        textColor: Colors.white,
                                        percent: '29',
                                        text0: 'Total Images'),
                                  ]
                                      .divide(const SizedBox(width: 3))
                                      .addToStart(const SizedBox(width: 16))
                                      .addToEnd(const SizedBox(width: 16)),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsetsDirectional.fromSTEB(
                                  16.0, 24.0, 16.0, 0.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  Expanded(
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      children: [
                                        Row(
                                          mainAxisSize: MainAxisSize.max,
                                          children: [
                                            Expanded(
                                              child: Text(
                                                'Recent Visits',
                                                style: FlutterFlowTheme.of(
                                                        context)
                                                    .labelLarge
                                                    .override(
                                                      fontFamily: 'Urbanist',
                                                      color:
                                                          FlutterFlowTheme.of(
                                                                  context)
                                                              .text,
                                                      letterSpacing: 0.0,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                    ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        ListView.builder(
                                          padding: const EdgeInsets.fromLTRB(
                                              0, 0, 0, 12),
                                          primary: false,
                                          shrinkWrap: true,
                                          scrollDirection: Axis.vertical,
                                          itemCount: dashboardController
                                              .visitelistResponse!
                                              .record!
                                              .length,
                                          itemBuilder: (context, index) {
                                            return Padding(
                                              padding: const EdgeInsets.only(
                                                  bottom: 12),
                                              child: VisitsCardWidgetDummy(
                                                record: dashboardController
                                                    .visitelistResponse!
                                                    .record![index],
                                              ),
                                            );
                                          },
                                        ),
                                      ].divide(const SizedBox(height: 12.0)),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ].addToEnd(const SizedBox(height: 16.0)),
                        ),
                      ),
                    ),
            );
          }),
    );
  }
}
