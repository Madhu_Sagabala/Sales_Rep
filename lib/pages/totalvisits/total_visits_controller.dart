import 'package:get/get.dart';
import 'package:sales_rep/backend/custom_response.dart';
import 'package:sales_rep/backend/services/total_visits_api.dart';
import 'package:sales_rep/models/total_visits_response.dart';

class TotalVisitsController extends GetxController {
  var visits = <Records>[].obs;
  var isLoading = true.obs;
  var hasError = false.obs;

  Future<void> fetchVisits() async {
    try {
      CustomResponse<TotalVisitsResponse> response =
          await TotalVisitsapi().call();
      if (response.statusCode == 200) {
        visits.value = response.data!.record!;
        isLoading.value = false;
        hasError.value = false;
      } else {
        isLoading.value = false;
        hasError.value = true;
      }
    } catch (e) {
      isLoading.value = false;
      hasError.value = true;
    }
  }
}
