import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sales_rep/Go_Route.dart';
import 'package:sales_rep/bottom_navigation.dart';
import 'package:sales_rep/flutter_flow/flutter_flow_util.dart';
import 'package:sales_rep/pages/totalvisits/total_visits_controller.dart';

import '/components/visits_card_widget.dart';
import '/flutter_flow/flutter_flow_theme.dart';

final TotalVisitsController controller = Get.put(TotalVisitsController());

class TotalvisitsWidget extends StatefulWidget {
  const TotalvisitsWidget({super.key});

  @override
  State<TotalvisitsWidget> createState() => _TotalvisitsWidgetState();
}

class _TotalvisitsWidgetState extends State<TotalvisitsWidget> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    controller.fetchVisits();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        key: scaffoldKey,
        backgroundColor: FlutterFlowTheme.of(context).tertiary,
        bottomNavigationBar: const BottomNavigation(activeIndex: 1),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            Get.toNamed(Routes.addVisit);
          },
          backgroundColor: const Color(0xFF005BFE),
          elevation: 8.0,
          child: Icon(
            Icons.add,
            color: FlutterFlowTheme.of(context).info,
            size: 32.0,
          ),
        ),
        body: SafeArea(
          top: true,
          child: Padding(
            padding: const EdgeInsetsDirectional.fromSTEB(16.0, 0.0, 16.0, 0.0),
            child: Column(
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(0.0),
                          child: SvgPicture.asset(
                            'assets/images/Frame_12.svg',
                            fit: BoxFit.cover,
                          ),
                        ),
                        Text(
                          'Visits',
                          style:
                              FlutterFlowTheme.of(context).titleMedium.override(
                                    fontFamily: 'Urbanist',
                                    color: FlutterFlowTheme.of(context).text,
                                    letterSpacing: 0.0,
                                    fontWeight: FontWeight.w500,
                                  ),
                        ),
                      ].divide(const SizedBox(width: 10.0)),
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Container(
                          decoration: const BoxDecoration(),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(8.0),
                            child: SvgPicture.asset(
                              'assets/images/SearchIcon.svg',
                              fit: BoxFit.cover,
                              color: FlutterFlowTheme.of(context).text,
                            ),
                          ),
                        ),
                        Container(
                          decoration: const BoxDecoration(),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(8.0),
                            child: SvgPicture.asset(
                              'assets/images/NotificationBellIcon.svg',
                              fit: BoxFit.cover,
                              color: FlutterFlowTheme.of(context).text,
                            ),
                          ),
                        ),
                      ].divide(const SizedBox(width: 10.0)),
                    ),
                  ],
                ),
                const SizedBox(height: 16.0),
                Obx(() {
                  if (controller.isLoading.value) {
                    return const CircularProgressIndicator();
                  } else if (controller.hasError.value) {
                    return const Text('Error loading visits');
                  } else {
                    return Expanded(
                      child: ListView.builder(
                        itemCount: controller.visits.length,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.only(bottom: 10),
                            child: VisitsCardWidget(
                                visitsData: controller.visits[index]),
                          );
                        },
                      ),
                    );
                  }
                }),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
