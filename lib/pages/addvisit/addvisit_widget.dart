import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sales_rep/Go_Route.dart';
import 'package:sales_rep/components/Textfield_datepicker.dart';
import 'package:sales_rep/components/textfield.dart';

import '/components/upload_image_widget.dart';
import '/flutter_flow/flutter_flow_drop_down.dart';
import '/flutter_flow/flutter_flow_icon_button.dart';
import '/flutter_flow/flutter_flow_theme.dart';
import '/flutter_flow/flutter_flow_util.dart';
import '/flutter_flow/flutter_flow_widgets.dart';
import '/flutter_flow/form_field_controller.dart';
import 'addvisit_model.dart';

export 'addvisit_model.dart';

class AddvisitWidget extends StatefulWidget {
  const AddvisitWidget({super.key});

  @override
  State<AddvisitWidget> createState() => _AddvisitWidgetState();
}

class _AddvisitWidgetState extends State<AddvisitWidget> {
  late AddvisitModel _model;
  String addvisitdate = '';

  final scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController Controller1 = TextEditingController();

  @override
  void initState() {
    super.initState();
    _model = createModel(context, () => AddvisitModel());

    _model.textController1 ??= TextEditingController();
    _model.textFieldFocusNode1 ??= FocusNode();

    _model.textController2 ??= TextEditingController();
    _model.textFieldFocusNode2 ??= FocusNode();

    _model.textController3 ??= TextEditingController();
    _model.textFieldFocusNode3 ??= FocusNode();
  }

  @override
  void dispose() {
    _model.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        child: Scaffold(
            key: scaffoldKey,
            backgroundColor: FlutterFlowTheme.of(context).primaryBackground,
            body: SafeArea(
                top: true,
                child: Padding(
                    padding: const EdgeInsetsDirectional.fromSTEB(16, 0, 16, 0),
                    child: SingleChildScrollView(
                        child: Column(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            FlutterFlowIconButton(
                              borderColor: const Color(0xFFE9E9E9),
                              borderRadius: 20,
                              borderWidth: 1,
                              buttonSize: 40,
                              fillColor: Colors.white,
                              icon: const Icon(
                                Icons.arrow_back_sharp,
                                color: Colors.black,
                                size: 20,
                              ),
                              onPressed: () {
                                Get.toNamed(Routes.totalVisits);
                              },
                            ),
                            Text(
                              'Add Visit',
                              style: FlutterFlowTheme.of(context)
                                  .titleMedium
                                  .override(
                                    fontFamily: 'Urbanist',
                                    color: FlutterFlowTheme.of(context)
                                        .primaryText,
                                    letterSpacing: 0,
                                    fontWeight: FontWeight.w500,
                                  ),
                            ),
                            Row(
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  Container(
                                      decoration: const BoxDecoration(),
                                      child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          child: SvgPicture.asset(
                                            'assets/images/NotificationBellIcon.svg',
                                            fit: BoxFit.cover,
                                            color: FlutterFlowTheme.of(context)
                                                .primaryText,
                                          )))
                                ].divide(const SizedBox(width: 10))),
                          ],
                        ),
                        Padding(
                          padding:
                              const EdgeInsetsDirectional.fromSTEB(0, 30, 0, 0),
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Expanded(
                                  child: Container(
                                      child: Column(
                                          mainAxisSize: MainAxisSize.max,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  right: 200),
                                              child: TextFeild_DatePicker(
                                                firstDate: DateTime.now(),
                                                dateCallback: (value) {
                                                  addvisitdate = value;
                                                },
                                                dateController: Controller1,
                                                lable: 'Date',
                                                errorKey: 'date_of_operation',
                                              ),
                                            ),
                                            Row(
                                              mainAxisSize: MainAxisSize.max,
                                              children: [
                                                Expanded(
                                                    child: Column(
                                                        mainAxisSize:
                                                            MainAxisSize.max,
                                                        children: [
                                                          Row(
                                                            mainAxisSize:
                                                                MainAxisSize
                                                                    .max,
                                                            children: [
                                                              Text('Place',
                                                                  style: FlutterFlowTheme.of(
                                                                          context)
                                                                      .bodyMedium
                                                                      .override(
                                                                        fontFamily:
                                                                            'Inter',
                                                                        color: FlutterFlowTheme.of(context)
                                                                            .primaryText,
                                                                        letterSpacing:
                                                                            0,
                                                                      )),
                                                            ],
                                                          ),
                                                          Row(
                                                            mainAxisSize:
                                                                MainAxisSize
                                                                    .max,
                                                            children: [
                                                              Expanded(
                                                                  child: FlutterFlowDropDown<
                                                                          String>(
                                                                      controller: _model
                                                                              .dropDownValueController ??=
                                                                          FormFieldController<String>(
                                                                              null),
                                                                      options: [
                                                                        'Option 1'
                                                                      ],
                                                                      onChanged: (val) =>
                                                                          setState(() => _model.dropDownValue =
                                                                              val),
                                                                      textStyle:
                                                                          FlutterFlowTheme.of(context)
                                                                              .bodyMedium
                                                                              .override(
                                                                                fontFamily: 'Urbanist',
                                                                                color: const Color(0xFFB6B6B6),
                                                                                letterSpacing: 0,
                                                                              ),
                                                                      hintText:
                                                                          'Select place',
                                                                      icon:
                                                                          Icon(
                                                                        Icons
                                                                            .keyboard_arrow_down_rounded,
                                                                        color: FlutterFlowTheme.of(context)
                                                                            .secondaryText,
                                                                        size:
                                                                            20,
                                                                      ),
                                                                      fillColor: FlutterFlowTheme.of(context)
                                                                          .secondaryBackground
                                                                          .withOpacity(
                                                                              0.4),
                                                                      elevation:
                                                                          2,
                                                                      borderColor:
                                                                          const Color(0xFFCDD4F4),
                                                                      borderWidth: 1,
                                                                      borderRadius: 8,
                                                                      margin: const EdgeInsetsDirectional.fromSTEB(10, 0, 10, 0),
                                                                      hidesUnderline: true,
                                                                      isOverButton: true,
                                                                      isSearchable: false,
                                                                      isMultiSelect: false))
                                                            ],
                                                          ),
                                                        ].divide(const SizedBox(
                                                            height: 8))))
                                              ],
                                            ),
                                            Row(
                                              mainAxisSize: MainAxisSize.max,
                                              children: [
                                                Expanded(
                                                    child: Column(
                                                        mainAxisSize:
                                                            MainAxisSize.max,
                                                        children: [
                                                          Row(
                                                            mainAxisSize:
                                                                MainAxisSize
                                                                    .max,
                                                            children: [
                                                              Text(
                                                                'TIme',
                                                                style: FlutterFlowTheme.of(
                                                                        context)
                                                                    .bodyMedium
                                                                    .override(
                                                                      fontFamily:
                                                                          'Inter',
                                                                      color: FlutterFlowTheme.of(
                                                                              context)
                                                                          .primaryText,
                                                                      letterSpacing:
                                                                          0,
                                                                    ),
                                                              ),
                                                            ],
                                                          ),
                                                          Row(
                                                              mainAxisSize:
                                                                  MainAxisSize
                                                                      .max,
                                                              children: [
                                                                Expanded(
                                                                  child:
                                                                      TextFormField(
                                                                    controller:
                                                                        _model
                                                                            .textController1,
                                                                    focusNode:
                                                                        _model
                                                                            .textFieldFocusNode1,
                                                                    autofocus:
                                                                        true,
                                                                    obscureText:
                                                                        false,
                                                                    decoration:
                                                                        InputDecoration(
                                                                      labelStyle: FlutterFlowTheme.of(context).labelMedium.override(
                                                                          fontFamily:
                                                                              'Urbanist',
                                                                          letterSpacing:
                                                                              0),
                                                                      hintText:
                                                                          'From',
                                                                      hintStyle: FlutterFlowTheme.of(context).labelMedium.override(
                                                                          fontFamily:
                                                                              'Urbanist',
                                                                          color: const Color(
                                                                              0xFFB6B6B6),
                                                                          letterSpacing:
                                                                              0),
                                                                      enabledBorder: OutlineInputBorder(
                                                                          borderSide: const BorderSide(
                                                                            color:
                                                                                Color(0xFFCDD4F4),
                                                                            width:
                                                                                1,
                                                                          ),
                                                                          borderRadius: BorderRadius.circular(8)),
                                                                      focusedBorder:
                                                                          OutlineInputBorder(
                                                                        borderSide:
                                                                            BorderSide(
                                                                          color:
                                                                              FlutterFlowTheme.of(context).primary,
                                                                          width:
                                                                              1,
                                                                        ),
                                                                        borderRadius:
                                                                            BorderRadius.circular(8),
                                                                      ),
                                                                      errorBorder: OutlineInputBorder(
                                                                          borderSide: BorderSide(
                                                                            color:
                                                                                FlutterFlowTheme.of(context).error,
                                                                            width:
                                                                                1,
                                                                          ),
                                                                          borderRadius: BorderRadius.circular(8)),
                                                                      focusedErrorBorder: OutlineInputBorder(
                                                                          borderSide: BorderSide(
                                                                            color:
                                                                                FlutterFlowTheme.of(context).error,
                                                                            width:
                                                                                1,
                                                                          ),
                                                                          borderRadius: BorderRadius.circular(8)),
                                                                      filled:
                                                                          true,
                                                                      fillColor: FlutterFlowTheme.of(
                                                                              context)
                                                                          .secondaryBackground
                                                                          .withOpacity(
                                                                              0.4),
                                                                    ),
                                                                    style: FlutterFlowTheme.of(
                                                                            context)
                                                                        .bodyMedium
                                                                        .override(
                                                                          fontFamily:
                                                                              'Urbanist',
                                                                          letterSpacing:
                                                                              0,
                                                                        ),
                                                                    validator: _model
                                                                        .textController1Validator
                                                                        .asValidator(
                                                                            context),
                                                                  ),
                                                                ),
                                                                Expanded(
                                                                  child:
                                                                      TextFormField(
                                                                    controller:
                                                                        _model
                                                                            .textController2,
                                                                    focusNode:
                                                                        _model
                                                                            .textFieldFocusNode2,
                                                                    autofocus:
                                                                        true,
                                                                    obscureText:
                                                                        false,
                                                                    decoration:
                                                                        InputDecoration(
                                                                      labelStyle: FlutterFlowTheme.of(context).labelMedium.override(
                                                                          fontFamily:
                                                                              'Urbanist',
                                                                          letterSpacing:
                                                                              0),
                                                                      hintText:
                                                                          'To',
                                                                      hintStyle: FlutterFlowTheme.of(context).labelMedium.override(
                                                                          fontFamily:
                                                                              'Urbanist',
                                                                          color: const Color(
                                                                              0xFFB6B6B6),
                                                                          letterSpacing:
                                                                              0),
                                                                      enabledBorder:
                                                                          OutlineInputBorder(
                                                                        borderSide:
                                                                            const BorderSide(
                                                                          color:
                                                                              Color(0xFFCDD4F4),
                                                                          width:
                                                                              1,
                                                                        ),
                                                                        borderRadius:
                                                                            BorderRadius.circular(8),
                                                                      ),
                                                                      focusedBorder:
                                                                          OutlineInputBorder(
                                                                        borderSide:
                                                                            BorderSide(
                                                                          color:
                                                                              FlutterFlowTheme.of(context).primary,
                                                                          width:
                                                                              1,
                                                                        ),
                                                                        borderRadius:
                                                                            BorderRadius.circular(8),
                                                                      ),
                                                                      errorBorder:
                                                                          OutlineInputBorder(
                                                                        borderSide:
                                                                            BorderSide(
                                                                          color:
                                                                              FlutterFlowTheme.of(context).error,
                                                                          width:
                                                                              1,
                                                                        ),
                                                                        borderRadius:
                                                                            BorderRadius.circular(8),
                                                                      ),
                                                                      focusedErrorBorder:
                                                                          OutlineInputBorder(
                                                                        borderSide:
                                                                            BorderSide(
                                                                          color:
                                                                              FlutterFlowTheme.of(context).error,
                                                                          width:
                                                                              1,
                                                                        ),
                                                                        borderRadius:
                                                                            BorderRadius.circular(8),
                                                                      ),
                                                                      filled:
                                                                          true,
                                                                      fillColor: FlutterFlowTheme.of(
                                                                              context)
                                                                          .secondaryBackground
                                                                          .withOpacity(
                                                                              0.4),
                                                                    ),
                                                                    style: FlutterFlowTheme.of(
                                                                            context)
                                                                        .bodyMedium
                                                                        .override(
                                                                          fontFamily:
                                                                              'Urbanist',
                                                                          letterSpacing:
                                                                              0,
                                                                        ),
                                                                    validator: _model
                                                                        .textController2Validator
                                                                        .asValidator(
                                                                            context),
                                                                  ),
                                                                ),
                                                              ].divide(
                                                                  const SizedBox(
                                                                      width:
                                                                          20))),
                                                        ].divide(const SizedBox(
                                                            height: 8))))
                                              ],
                                            ),
                                            Row(
                                              mainAxisSize: MainAxisSize.max,
                                              children: [
                                                Expanded(
                                                  child: Column(
                                                    mainAxisSize:
                                                        MainAxisSize.max,
                                                    children: [
                                                      Row(
                                                        mainAxisSize:
                                                            MainAxisSize.max,
                                                        children: [
                                                          Expanded(
                                                              child: Column(
                                                                  mainAxisSize:
                                                                      MainAxisSize
                                                                          .max,
                                                                  children: [
                                                                    Row(
                                                                      mainAxisSize:
                                                                          MainAxisSize
                                                                              .max,
                                                                      children: [
                                                                        Text(
                                                                            'Image',
                                                                            style: FlutterFlowTheme.of(context).bodyMedium.override(
                                                                                fontFamily: 'Inter',
                                                                                color: FlutterFlowTheme.of(context).primaryText,
                                                                                letterSpacing: 0))
                                                                      ],
                                                                    ),
                                                                    Row(
                                                                      mainAxisSize:
                                                                          MainAxisSize
                                                                              .max,
                                                                      children: [
                                                                        FFButtonWidget(
                                                                            onPressed:
                                                                                () async {
                                                                              await showModalBottomSheet(
                                                                                isScrollControlled: true,
                                                                                backgroundColor: Colors.transparent,
                                                                                enableDrag: false,
                                                                                context: context,
                                                                                builder: (context) {
                                                                                  return GestureDetector(onTap: () => _model.unfocusNode.canRequestFocus ? FocusScope.of(context).requestFocus(_model.unfocusNode) : FocusScope.of(context).unfocus(), child: Padding(padding: MediaQuery.viewInsetsOf(context), child: const UploadImageWidget()));
                                                                                },
                                                                              ).then((value) => safeSetState(() {}));
                                                                            },
                                                                            text:
                                                                                'Upload',
                                                                            icon: const Icon(Icons.link_rounded,
                                                                                size:
                                                                                    15),
                                                                            options: FFButtonOptions(
                                                                                height: 40,
                                                                                padding: const EdgeInsetsDirectional.fromSTEB(21, 12, 21, 12),
                                                                                iconPadding: const EdgeInsetsDirectional.fromSTEB(0, 0, 0, 0),
                                                                                color: FlutterFlowTheme.of(context).secondaryBackground,
                                                                                textStyle: FlutterFlowTheme.of(context).titleSmall.override(fontFamily: 'Urbanist', color: const Color(0xFF005BFE), letterSpacing: 0),
                                                                                elevation: 0,
                                                                                borderSide: const BorderSide(color: Color(0xFFCDD4F4), width: 1),
                                                                                borderRadius: BorderRadius.circular(8))),
                                                                      ],
                                                                    ),
                                                                  ].divide(const SizedBox(
                                                                      height:
                                                                          12))))
                                                        ],
                                                      ),
                                                      Column(
                                                        mainAxisSize:
                                                            MainAxisSize.max,
                                                        children: [
                                                          Row(
                                                            mainAxisSize:
                                                                MainAxisSize
                                                                    .max,
                                                            children: [
                                                              Expanded(
                                                                child:
                                                                    Container(
                                                                  decoration: BoxDecoration(
                                                                      color: FlutterFlowTheme.of(
                                                                              context)
                                                                          .secondaryBackground,
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              8),
                                                                      border: Border.all(
                                                                          color:
                                                                              const Color(0xFFCDD4F4))),
                                                                  child:
                                                                      Padding(
                                                                    padding:
                                                                        const EdgeInsets
                                                                            .all(
                                                                            8),
                                                                    child: Row(
                                                                      mainAxisSize:
                                                                          MainAxisSize
                                                                              .max,
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      children: [
                                                                        Row(
                                                                          mainAxisSize:
                                                                              MainAxisSize.max,
                                                                          children:
                                                                              [
                                                                            Container(
                                                                              decoration: const BoxDecoration(),
                                                                              child: ClipRRect(
                                                                                borderRadius: BorderRadius.circular(8),
                                                                                child: SvgPicture.asset(
                                                                                  'assets/images/ImageIconUploading.svg',
                                                                                  fit: BoxFit.cover,
                                                                                  color: FlutterFlowTheme.of(context).secondaryText,
                                                                                ),
                                                                              ),
                                                                            ),
                                                                            Column(
                                                                              mainAxisSize: MainAxisSize.max,
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              children: [
                                                                                Text(
                                                                                  'Image-202.png',
                                                                                  style: FlutterFlowTheme.of(context).bodyMedium.override(
                                                                                        fontFamily: 'Inter',
                                                                                        color: FlutterFlowTheme.of(context).secondaryText,
                                                                                        fontSize: 12,
                                                                                        letterSpacing: 0,
                                                                                      ),
                                                                                ),
                                                                                Text(
                                                                                  '2.3 mb',
                                                                                  style: FlutterFlowTheme.of(context).bodySmall.override(
                                                                                        fontFamily: 'Urbanist',
                                                                                        fontSize: 10,
                                                                                        letterSpacing: 0,
                                                                                        fontWeight: FontWeight.w500,
                                                                                      ),
                                                                                ),
                                                                              ].divide(const SizedBox(height: 4)),
                                                                            ),
                                                                          ].divide(const SizedBox(width: 8)),
                                                                        ),
                                                                        Icon(
                                                                          Icons
                                                                              .close,
                                                                          color:
                                                                              FlutterFlowTheme.of(context).secondaryText,
                                                                          size:
                                                                              18,
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                          Row(
                                                            mainAxisSize:
                                                                MainAxisSize
                                                                    .max,
                                                            children: [
                                                              Expanded(
                                                                  child: Container(
                                                                      decoration: BoxDecoration(
                                                                        color: FlutterFlowTheme.of(context)
                                                                            .secondaryBackground,
                                                                        borderRadius:
                                                                            BorderRadius.circular(8),
                                                                        border:
                                                                            Border.all(
                                                                          color:
                                                                              const Color(0xFFCDD4F4),
                                                                        ),
                                                                      ),
                                                                      child: Padding(
                                                                          padding: const EdgeInsets.all(8),
                                                                          child: Row(
                                                                            mainAxisSize:
                                                                                MainAxisSize.max,
                                                                            mainAxisAlignment:
                                                                                MainAxisAlignment.spaceBetween,
                                                                            children: [
                                                                              Row(
                                                                                mainAxisSize: MainAxisSize.max,
                                                                                children: [
                                                                                  Container(
                                                                                    decoration: const BoxDecoration(),
                                                                                    child: ClipRRect(
                                                                                      borderRadius: BorderRadius.circular(8),
                                                                                      child: SvgPicture.asset(
                                                                                        'assets/images/ImageIconUploading.svg',
                                                                                        fit: BoxFit.cover,
                                                                                        color: FlutterFlowTheme.of(context).secondaryText,
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                  Column(
                                                                                    mainAxisSize: MainAxisSize.max,
                                                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                                                    children: [
                                                                                      Text(
                                                                                        'Image-202.png',
                                                                                        style: FlutterFlowTheme.of(context).bodyMedium.override(
                                                                                              fontFamily: 'Inter',
                                                                                              color: FlutterFlowTheme.of(context).secondaryText,
                                                                                              fontSize: 12,
                                                                                              letterSpacing: 0,
                                                                                            ),
                                                                                      ),
                                                                                      Text(
                                                                                        '2.3 mb',
                                                                                        style: FlutterFlowTheme.of(context).bodySmall.override(
                                                                                              fontFamily: 'Urbanist',
                                                                                              fontSize: 10,
                                                                                              letterSpacing: 0,
                                                                                              fontWeight: FontWeight.w500,
                                                                                            ),
                                                                                      ),
                                                                                    ].divide(const SizedBox(height: 4)),
                                                                                  ),
                                                                                ].divide(const SizedBox(width: 8)),
                                                                              ),
                                                                              Icon(Icons.close, color: FlutterFlowTheme.of(context).secondaryText, size: 18),
                                                                            ],
                                                                          ))))
                                                            ],
                                                          ),
                                                        ].divide(const SizedBox(
                                                            height: 8)),
                                                      ),
                                                    ].divide(const SizedBox(
                                                        height: 12)),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Row(
                                              mainAxisSize: MainAxisSize.max,
                                              children: [
                                                Expanded(
                                                  child: Column(
                                                    mainAxisSize:
                                                        MainAxisSize.max,
                                                    children: [
                                                      Row(
                                                        mainAxisSize:
                                                            MainAxisSize.max,
                                                        children: [
                                                          Text(
                                                            'Note',
                                                            style: FlutterFlowTheme
                                                                    .of(context)
                                                                .bodyMedium
                                                                .override(
                                                                  fontFamily:
                                                                      'Inter',
                                                                  color: FlutterFlowTheme.of(
                                                                          context)
                                                                      .primaryText,
                                                                  letterSpacing:
                                                                      0,
                                                                ),
                                                          ),
                                                        ],
                                                      ),
                                                      Row(
                                                        mainAxisSize:
                                                            MainAxisSize.max,
                                                        children: [
                                                          Expanded(
                                                              child:
                                                                  TextFieldComponent(
                                                            controller: _model
                                                                .textController3!,
                                                            maxlines: 3,
                                                            hintText:
                                                                'Write...',
                                                          )),
                                                        ],
                                                      ),
                                                    ].divide(const SizedBox(
                                                        height: 8)),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ].divide(
                                              const SizedBox(height: 20)))))
                            ],
                          ),
                        ),
                        Padding(
                            padding: const EdgeInsetsDirectional.fromSTEB(
                                0, 40, 0, 0),
                            child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  FFButtonWidget(
                                    onPressed: () {
                                      print('Button pressed ...');
                                    },
                                    text: 'Cancel',
                                    options: FFButtonOptions(
                                      height: 40,
                                      padding:
                                          const EdgeInsetsDirectional.fromSTEB(
                                              30, 10, 30, 10),
                                      iconPadding:
                                          const EdgeInsetsDirectional.fromSTEB(
                                              0, 0, 0, 0),
                                      color: Colors.white,
                                      textStyle: FlutterFlowTheme.of(context)
                                          .titleSmall
                                          .override(
                                              fontFamily: 'Urbanist',
                                              color: const Color(0xFF005BFE),
                                              letterSpacing: 0),
                                      elevation: 0,
                                      borderSide: const BorderSide(
                                        width: 0,
                                      ),
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                  ),
                                  FFButtonWidget(
                                      onPressed: () {
                                        print('Button pressed ...');
                                      },
                                      text: 'Submit',
                                      options: FFButtonOptions(
                                          height: 40,
                                          padding: const EdgeInsetsDirectional
                                              .fromSTEB(30, 10, 30, 10),
                                          iconPadding:
                                              const EdgeInsetsDirectional
                                                  .fromSTEB(0, 0, 0, 0),
                                          color: const Color(0xFF005BFE),
                                          textStyle:
                                              FlutterFlowTheme.of(context)
                                                  .titleSmall
                                                  .override(
                                                      fontFamily: 'Urbanist',
                                                      color: Colors.white,
                                                      letterSpacing: 0),
                                          elevation: 0,
                                          borderSide:
                                              const BorderSide(width: 0),
                                          borderRadius:
                                              BorderRadius.circular(8)))
                                ].divide(const SizedBox(width: 16))))
                      ],
                    ))))));
  }
}
