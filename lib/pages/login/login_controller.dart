import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:sales_rep/GetStorage/Get_Storage.dart';
import 'package:sales_rep/backend/app_response.dart';
import 'package:sales_rep/backend/custom_response.dart';
import 'package:sales_rep/backend/services/login_api.dart';
import 'package:sales_rep/models/login_response.dart';

class LoginController extends GetxController {
  var email = ''.obs;
  var password = ''.obs;
  var isVisible = false.obs;
  var error = false.obs;
  var isValidation = false.obs;
  var message = ''.obs;
  var user = ''.obs;
  var appResponse = Rxn<AppResponse>();

  TextEditingController controller1 = TextEditingController();
  TextEditingController controller2 = TextEditingController();
  var passwordVisibility = false.obs;

  void toggleVisibility() {
    isVisible.value = !isVisible.value;
    update();
  }

  Future<void> signIn({required String email, required String password}) async {
    CustomResponse<UserResponse> response =
        await SignInApi().call(email: email, password: password);

    error.value = false;
    isValidation.value = false;
    message.value = '';
    appResponse.value = AppResponse(response: response);

    switch (response.statusCode) {
      case 200:
        message.value = response.data!.message;

        GetStoragereference.setAccessToken(
            response.data!.data!.accessToken.toString());

        message.value = response.data!.message;
        user.value = response.data!.data!.userDetails.fullName;

        break;

      case 422:
        message.value = response.data!.message;
        isValidation.value = true;
        error.value = true;
        break;

      case 401:
        message.value = response.data!.message;
        isValidation.value = false;
        error.value = true;
        break;

      default:
        message.value = response.data!.message.toString();
        error.value = true;
    }
  }
}
