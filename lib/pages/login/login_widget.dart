import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sales_rep/Go_Route.dart';
import 'package:sales_rep/core/form_text_field.dart';
import 'package:sales_rep/pages/login/login_controller.dart';
import 'package:sales_rep/snackbars/success_snackbar.dart';

import '/flutter_flow/flutter_flow_theme.dart';
import '/flutter_flow/flutter_flow_widgets.dart';

class LoginWidget extends StatelessWidget {
  const LoginWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final LoginController controller = Get.put(LoginController());
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        backgroundColor: FlutterFlowTheme.of(context).primaryBackground,
        body: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Padding(
              padding:
                  const EdgeInsetsDirectional.fromSTEB(0.0, 48.0, 0.0, 48.0),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(0.0),
                    child: SvgPicture.asset(
                      'assets/images/Wave.svg',
                      fit: BoxFit.cover,
                    ),
                  ),
                ],
              ),
            ),
            Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Padding(
                        padding: const EdgeInsetsDirectional.fromSTEB(
                            20.0, 0.0, 20.0, 0.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Text(
                                  'Sign In',
                                  style: FlutterFlowTheme.of(context)
                                      .headlineSmall
                                      .override(
                                          fontFamily: 'Urbanist',
                                          letterSpacing: 0.0,
                                          color: FlutterFlowTheme.of(context)
                                              .text),
                                ),
                              ],
                            ),
                            const SizedBox(height: 14.0),
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Flexible(
                                  child: Text(
                                    'Welcome to SalesTracker Step into a world of efficiency and empowerment.',
                                    style: FlutterFlowTheme.of(context)
                                        .bodyMedium
                                        .override(
                                          fontFamily: 'Urbanist',
                                          color: FlutterFlowTheme.of(context)
                                              .secondaryText,
                                          letterSpacing: 0.0,
                                        ),
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 24.0),
                            Column(
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Obx(() => FormTextField(
                                      controller: controller.controller1,
                                      hintText: 'E-mail',
                                      errorKey: 'email',
                                      errors:
                                          controller.appResponse.value?.errors,
                                    )),
                                const SizedBox(height: 20.0),
                                Obx(() => FormTextField(
                                    obscureText: !controller.isVisible.value,
                                    controller: controller.controller2,
                                    maxLine: 1,
                                    color:
                                        FlutterFlowTheme.of(context).boderColor,
                                    errors:
                                        controller.appResponse.value?.errors,
                                    errorKey: 'password',
                                    hintText: 'Password',
                                    suffixIcon: IconButton(
                                        icon: Icon(controller.isVisible.value
                                            ? Icons.visibility
                                            : Icons.visibility_off),
                                        onPressed:
                                            controller.toggleVisibility))),
                                const SizedBox(height: 20.0),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Text('Forget Password?',
                                        style: FlutterFlowTheme.of(context)
                                            .bodyMedium
                                            .override(
                                                fontFamily: 'Urbanist',
                                                color:
                                                    FlutterFlowTheme.of(context)
                                                        .smalltext,
                                                letterSpacing: 0.0,
                                                fontWeight: FontWeight.w500))
                                  ],
                                ),
                                const SizedBox(height: 10),
                                Obx(() => (controller.error.value &&
                                        controller.message.isNotEmpty &&
                                        !controller.isValidation.value)
                                    ? Padding(
                                        padding: const EdgeInsets.only(left: 0),
                                        child: (Text(controller.message.value,
                                            style: const TextStyle(
                                                color: Colors.red,
                                                fontFamily: 'Urbanist',
                                                fontSize: 16))))
                                    : Container()),
                                const SizedBox(height: 16),
                                FFButtonWidget(
                                    onPressed: () async {
                                      await controller.signIn(
                                          email: controller
                                              .controller1.value.text
                                              .trim(),
                                          password: controller
                                              .controller2.value.text
                                              .trim());
                                      if (controller.error.value &&
                                          controller.message.value.isNotEmpty) {
                                        return;
                                      } else if (!controller.error.value &&
                                          controller.message.value.isNotEmpty) {
                                        getsuccesssnackbar(
                                            message: controller.message.value);
                                        controller.controller1.clear();
                                        controller.controller2.clear();
                                        Get.offNamed(Routes.dashboard);
                                      }
                                    },
                                    text: 'Log In',
                                    options: FFButtonOptions(
                                        height: 40.0,
                                        width: 370,
                                        padding: const EdgeInsetsDirectional
                                            .fromSTEB(9.0, 0.0, 9.0, 0.0),
                                        iconPadding: const EdgeInsetsDirectional
                                            .fromSTEB(0.0, 0.0, 0.0, 0.0),
                                        color: const Color(0xFF005BFE),
                                        textStyle: FlutterFlowTheme.of(context)
                                            .labelMedium
                                            .override(
                                              fontFamily: 'Urbanist',
                                              color: Colors.white,
                                              letterSpacing: 0.0,
                                              fontWeight: FontWeight.w600,
                                            ),
                                        elevation: 0.0,
                                        borderSide: const BorderSide(
                                            color: Colors.transparent,
                                            width: 1.0),
                                        borderRadius:
                                            BorderRadius.circular(8.0))),
                                const SizedBox(height: 24.0),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text('Don’t have account?',
                                        style: FlutterFlowTheme.of(context)
                                            .bodyMedium
                                            .override(
                                                fontFamily: 'Urbanist',
                                                color:
                                                    FlutterFlowTheme.of(context)
                                                        .passwordfield,
                                                letterSpacing: 0.0)),
                                    const SizedBox(width: 5.0),
                                    Text('Sign Up',
                                        style: FlutterFlowTheme.of(context)
                                            .bodyMedium
                                            .override(
                                                fontFamily: 'Urbanist',
                                                color: const Color(0xFF005BFE),
                                                letterSpacing: 0.0,
                                                fontWeight: FontWeight.w600))
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            const Spacer(),
            Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                    child: Container(
                        height: 230,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                image: Image.asset(
                                  'assets/images/Vector_1.png',
                                ).image))))
              ],
            ),
          ],
        ),
      ),
    );
  }
}
