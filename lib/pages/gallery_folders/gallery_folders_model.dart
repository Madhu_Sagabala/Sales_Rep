import '/components/folder_widget.dart';
import '/flutter_flow/flutter_flow_util.dart';
import 'gallery_folders_widget.dart' show GalleryFoldersWidget;
import 'package:flutter/material.dart';

class GalleryFoldersModel extends FlutterFlowModel<GalleryFoldersWidget> {
  ///  State fields for stateful widgets in this page.

  final unfocusNode = FocusNode();
  // Model for Folder component.
  late FolderModel folderModel1;
  // Model for Folder component.
  late FolderModel folderModel2;
  // Model for Folder component.
  late FolderModel folderModel3;
  // Model for Folder component.
  late FolderModel folderModel4;
  // Model for Folder component.
  late FolderModel folderModel5;
  // Model for Folder component.
  late FolderModel folderModel6;
  // Model for Folder component.
  late FolderModel folderModel7;
  // Model for Folder component.
  late FolderModel folderModel8;

  @override
  void initState(BuildContext context) {
    folderModel1 = createModel(context, () => FolderModel());
    folderModel2 = createModel(context, () => FolderModel());
    folderModel3 = createModel(context, () => FolderModel());
    folderModel4 = createModel(context, () => FolderModel());
    folderModel5 = createModel(context, () => FolderModel());
    folderModel6 = createModel(context, () => FolderModel());
    folderModel7 = createModel(context, () => FolderModel());
    folderModel8 = createModel(context, () => FolderModel());
  }

  @override
  void dispose() {
    unfocusNode.dispose();
    folderModel1.dispose();
    folderModel2.dispose();
    folderModel3.dispose();
    folderModel4.dispose();
    folderModel5.dispose();
    folderModel6.dispose();
    folderModel7.dispose();
    folderModel8.dispose();
  }
}
