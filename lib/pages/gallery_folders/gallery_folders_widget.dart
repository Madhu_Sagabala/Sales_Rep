import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sales_rep/Go_Route.dart';
import 'package:sales_rep/bottom_navigation.dart';

import '/components/folder_widget.dart';
import '/flutter_flow/flutter_flow_theme.dart';
import '/flutter_flow/flutter_flow_util.dart';
import 'gallery_folders_model.dart';

export 'gallery_folders_model.dart';

class GalleryFoldersWidget extends StatefulWidget {
  const GalleryFoldersWidget({super.key});

  @override
  State<GalleryFoldersWidget> createState() => _GalleryFoldersWidgetState();
}

class _GalleryFoldersWidgetState extends State<GalleryFoldersWidget> {
  late GalleryFoldersModel _model;

  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _model = createModel(context, () => GalleryFoldersModel());
  }

  @override
  void dispose() {
    _model.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => _model.unfocusNode.canRequestFocus
          ? FocusScope.of(context).requestFocus(_model.unfocusNode)
          : FocusScope.of(context).unfocus(),
      child: Scaffold(
        key: scaffoldKey,
        backgroundColor: FlutterFlowTheme.of(context).tertiary,
        bottomNavigationBar: const BottomNavigation(activeIndex: 3),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            print('FloatingActionButton pressed ...');
          },
          backgroundColor: FlutterFlowTheme.of(context).primary,
          elevation: 8.0,
          child: Icon(
            Icons.photo_camera_outlined,
            color: FlutterFlowTheme.of(context).info,
            size: 32.0,
          ),
        ),
        body: SafeArea(
          top: true,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: Stack(
                  alignment: const AlignmentDirectional(0.0, -1.0),
                  children: [
                    SingleChildScrollView(
                      primary: false,
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Padding(
                            padding: const EdgeInsetsDirectional.fromSTEB(
                                16.0, 0.0, 16.0, 0.0),
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: [
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(0.0),
                                      child: SvgPicture.asset(
                                        'assets/images/Frame_12.svg',
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    Text(
                                      'Gallery',
                                      style: FlutterFlowTheme.of(context)
                                          .titleMedium
                                          .override(
                                            fontFamily: 'Urbanist',
                                            color: Colors.black,
                                            letterSpacing: 0.0,
                                            fontWeight: FontWeight.w500,
                                          ),
                                    ),
                                  ].divide(const SizedBox(width: 10.0)),
                                ),
                                Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: [
                                    Container(
                                      decoration: const BoxDecoration(),
                                      child: ClipRRect(
                                        borderRadius:
                                            BorderRadius.circular(8.0),
                                        child: SvgPicture.asset(
                                          'assets/images/SearchIcon.svg',
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      decoration: const BoxDecoration(),
                                      child: ClipRRect(
                                        borderRadius:
                                            BorderRadius.circular(8.0),
                                        child: SvgPicture.asset(
                                          'assets/images/NotificationBellIcon.svg',
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                  ].divide(const SizedBox(width: 10.0)),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsetsDirectional.fromSTEB(
                                16.0, 24.0, 16.0, 0.0),
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                ListView(
                                  padding: EdgeInsets.zero,
                                  shrinkWrap: true,
                                  scrollDirection: Axis.vertical,
                                  children: [
                                    wrapWithModel(
                                      model: _model.folderModel1,
                                      updateCallback: () => setState(() {}),
                                      child: const FolderWidget(),
                                    ),
                                    wrapWithModel(
                                      model: _model.folderModel2,
                                      updateCallback: () => setState(() {}),
                                      child: const FolderWidget(),
                                    ),
                                    wrapWithModel(
                                      model: _model.folderModel3,
                                      updateCallback: () => setState(() {}),
                                      child: const FolderWidget(),
                                    ),
                                    wrapWithModel(
                                      model: _model.folderModel4,
                                      updateCallback: () => setState(() {}),
                                      child: const FolderWidget(),
                                    ),
                                    wrapWithModel(
                                      model: _model.folderModel5,
                                      updateCallback: () => setState(() {}),
                                      child: const FolderWidget(),
                                    ),
                                    wrapWithModel(
                                      model: _model.folderModel6,
                                      updateCallback: () => setState(() {}),
                                      child: const FolderWidget(),
                                    ),
                                    wrapWithModel(
                                      model: _model.folderModel7,
                                      updateCallback: () => setState(() {}),
                                      child: const FolderWidget(),
                                    ),
                                    wrapWithModel(
                                      model: _model.folderModel8,
                                      updateCallback: () => setState(() {}),
                                      child: const FolderWidget(),
                                    ),
                                  ].divide(const SizedBox(height: 16.0)),
                                ),
                              ],
                            ),
                          ),
                        ].addToEnd(const SizedBox(height: 24.0)),
                      ),
                    ),
                    Align(
                      alignment: const AlignmentDirectional(0.0, 0.94),
                      child: Container(
                        decoration: const BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              blurRadius: 9.0,
                              color: Color(0x15000000),
                              offset: Offset(
                                0.0,
                                1.0,
                              ),
                            )
                          ],
                        ),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            InkWell(
                              splashColor: Colors.transparent,
                              focusColor: Colors.transparent,
                              hoverColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              onTap: () async {
                                Get.toNamed(Routes.gallery);
                              },
                              child: Container(
                                decoration: const BoxDecoration(
                                  color: Color(0xB4FFFFFF),
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(30.0),
                                    bottomRight: Radius.circular(0.0),
                                    topLeft: Radius.circular(30.0),
                                    topRight: Radius.circular(0.0),
                                  ),
                                ),
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: [
                                    Padding(
                                      padding:
                                          const EdgeInsetsDirectional.fromSTEB(
                                              16.0, 6.0, 16.0, 6.0),
                                      child: Text(
                                        'Gallery',
                                        style: FlutterFlowTheme.of(context)
                                            .bodyMedium
                                            .override(
                                              fontFamily: 'Urbanist',
                                              color: const Color(0xB60D0D0F),
                                              fontSize: 12.0,
                                              letterSpacing: 0.0,
                                              fontWeight: FontWeight.w500,
                                            ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                color: FlutterFlowTheme.of(context)
                                    .secondaryBackground,
                                borderRadius: const BorderRadius.only(
                                  bottomLeft: Radius.circular(0.0),
                                  bottomRight: Radius.circular(30.0),
                                  topLeft: Radius.circular(0.0),
                                  topRight: Radius.circular(30.0),
                                ),
                                border: Border.all(
                                  color: const Color(0xFFC3C3C3),
                                  width: 1.0,
                                ),
                              ),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  Padding(
                                    padding:
                                        const EdgeInsetsDirectional.fromSTEB(
                                            16.0, 6.0, 16.0, 6.0),
                                    child: Text(
                                      'Folders',
                                      style: FlutterFlowTheme.of(context)
                                          .bodyMedium
                                          .override(
                                            fontFamily: 'Urbanist',
                                            color: FlutterFlowTheme.of(context)
                                                .secondaryText,
                                            fontSize: 12.0,
                                            letterSpacing: 0.0,
                                            fontWeight: FontWeight.w500,
                                          ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ].addToStart(const SizedBox(width: 0.0)),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
