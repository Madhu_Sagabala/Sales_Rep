import 'package:get/get.dart';
import 'package:sales_rep/backend/custom_response.dart';
import 'package:sales_rep/backend/services/total_location_api.dart';
import 'package:sales_rep/models/total_location_response.dart';

class TotalLocationController extends GetxController {
  var location = <LocationRecord>[].obs;
  var isLoading = true.obs;
  var hasError = false.obs;

  Future<void> fetchLocations() async {
    try {
      CustomResponse<TotalLocationsResponse> response =
          await TotalLocationsapi().call();
      if (response.statusCode == 200) {
        location.value = response.data!.record!;
        isLoading.value = false;
        hasError.value = false;
      } else {
        isLoading.value = false;
        hasError.value = true;
      }
    } catch (e) {
      isLoading.value = false;
      hasError.value = true;
    }
  }
}
