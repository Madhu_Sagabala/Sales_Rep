import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sales_rep/Go_Route.dart';
import 'package:sales_rep/bottom_navigation.dart';
import 'package:sales_rep/components/location_card_widget.dart';
import 'package:sales_rep/pages/total_locations/total_location_controller.dart';

import '/flutter_flow/flutter_flow_theme.dart';
import '/flutter_flow/flutter_flow_util.dart';

final TotalLocationController controller = Get.put(TotalLocationController());

class TotalLocationsWidget extends StatefulWidget {
  TotalLocationsWidget({super.key});

  @override
  State<TotalLocationsWidget> createState() => _TotalLocationsWidgetState();
}

class _TotalLocationsWidgetState extends State<TotalLocationsWidget> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    controller.fetchLocations();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Scaffold(
        key: scaffoldKey,
        backgroundColor: FlutterFlowTheme.of(context).tertiary,
        bottomNavigationBar: const BottomNavigation(activeIndex: 2),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            Get.toNamed(Routes.addLocation);
          },
          backgroundColor: const Color(0xFF005BFE),
          elevation: 8.0,
          child: Icon(
            Icons.add,
            color: FlutterFlowTheme.of(context).info,
            size: 32.0,
          ),
        ),
        body: SafeArea(
          top: true,
          child: Padding(
            padding: const EdgeInsetsDirectional.fromSTEB(16.0, 0.0, 16.0, 0.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(0.0),
                          child: SvgPicture.asset(
                            'assets/images/Frame_12.svg',
                            fit: BoxFit.cover,
                          ),
                        ),
                        Text(
                          'Location',
                          style:
                              FlutterFlowTheme.of(context).titleMedium.override(
                                    fontFamily: 'Urbanist',
                                    color: FlutterFlowTheme.of(context).text,
                                    letterSpacing: 0.0,
                                    fontWeight: FontWeight.w500,
                                  ),
                        ),
                      ].divide(const SizedBox(width: 10.0)),
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Container(
                          decoration: const BoxDecoration(),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(8.0),
                            child: SvgPicture.asset(
                              'assets/images/SearchIcon.svg',
                              fit: BoxFit.cover,
                              color: FlutterFlowTheme.of(context).text,
                            ),
                          ),
                        ),
                        Container(
                          decoration: const BoxDecoration(),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(8.0),
                            child: SvgPicture.asset(
                              'assets/images/NotificationBellIcon.svg',
                              fit: BoxFit.cover,
                              color: FlutterFlowTheme.of(context).text,
                            ),
                          ),
                        ),
                      ].divide(const SizedBox(width: 10.0)),
                    ),
                  ],
                ),
                Obx(() {
                  if (controller.isLoading.value) {
                    return const CircularProgressIndicator();
                  } else if (controller.hasError.value) {
                    return const Text('No-Connection');
                  } else {
                    if (controller.location.isEmpty) {
                      return const Text('No locations found');
                    }
                    return Expanded(
                      child: ListView.builder(
                          itemCount: controller.location.length,
                          itemBuilder: ((context, index) {
                            return Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: LocationCardWidget(
                                  locationCard: controller.location[index]),
                            );
                          })),
                    );
                  }
                })
              ].addToEnd(const SizedBox(height: 16.0)),
            ),
          ),
        ),
      ),
    );
  }
}
