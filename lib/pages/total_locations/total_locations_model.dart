import '/flutter_flow/flutter_flow_util.dart';
import 'total_locations_widget.dart' show TotalLocationsWidget;
import 'package:flutter/material.dart';

class TotalLocationsModel extends FlutterFlowModel<TotalLocationsWidget> {
  ///  State fields for stateful widgets in this page.

  final unfocusNode = FocusNode();


  @override
  void initState(BuildContext context) {
  
  }

  @override
  void dispose() {
    unfocusNode.dispose();
  }
}
