import '/flutter_flow/flutter_flow_util.dart';
import 'folders_widget.dart' show FoldersWidget;
import 'package:flutter/material.dart';

class FoldersModel extends FlutterFlowModel<FoldersWidget> {
  ///  State fields for stateful widgets in this page.

  final unfocusNode = FocusNode();

  @override
  void initState(BuildContext context) {}

  @override
  void dispose() {
    unfocusNode.dispose();
  }
}
