import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sales_rep/GetStorage/Get_Storage.dart';
import 'package:sales_rep/Go_Route.dart';
import 'package:sales_rep/models/visite_list_model.dart';
import 'package:sales_rep/pages/dashboard/dashboard_widget.dart';
import 'package:sales_rep/pages/gallery/gallery_widget.dart';
import 'package:sales_rep/pages/total_locations/total_locations_widget.dart';
import 'package:sales_rep/profile/profile_widget.dart';
import 'package:sales_rep/view_visit/view_visit_widget.dart';

import '/flutter_flow/flutter_flow_theme.dart';
import '/flutter_flow/flutter_flow_util.dart';

class VisitsCardWidgetDummy extends StatefulWidget {
  final Record? record;
  const VisitsCardWidgetDummy({super.key, this.record});

  @override
  State<VisitsCardWidgetDummy> createState() => _VisitsCardWidgetDummyState();
}

class _VisitsCardWidgetDummyState extends State<VisitsCardWidgetDummy> {
  final List<Widget> pages = [
    const DashboardWidget(),
    const ViewVisitWidget(),
    TotalLocationsWidget(),
    const GalleryWidget(),
    const ProfileWidget(),
  ];
  @override
  void setState(VoidCallback callback) {
    super.setState(callback);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Colors.transparent,
      focusColor: Colors.transparent,
      hoverColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: () async {
        GetStoragereference.setvisiteId(widget.record!.id!);
        Get.toNamed(Routes.viewvisit);
      },
      child: Container(
        decoration: BoxDecoration(
          color: FlutterFlowTheme.of(context).visitCardColor,
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                decoration: BoxDecoration(
                  color: FlutterFlowTheme.of(context).white,
                  shape: BoxShape.circle,
                ),
                child: Padding(
                  padding: const EdgeInsets.all(0.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(40.0),
                    child: Image.network(
                      height: 40,
                      width: 40,
                      widget.record!.images!.first,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Column(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Text(
                              dateTimeFormat('MMM d ,yyy', widget.record!.date),
                              style: FlutterFlowTheme.of(context)
                                  .bodyMedium
                                  .override(
                                    fontFamily: 'Urbanist',
                                    color: FlutterFlowTheme.of(context)
                                        .primaryText,
                                    letterSpacing: 0.0,
                                  ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Text(
                              widget.record!.place.toString(),
                              style: FlutterFlowTheme.of(context)
                                  .labelLarge
                                  .override(
                                    fontFamily: 'Urbanist',
                                    color: FlutterFlowTheme.of(context)
                                        .primaryText,
                                    letterSpacing: 0.0,
                                  ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            const Icon(
                              Icons.access_time_filled_sharp,
                              color: Color(0xFF565656),
                              size: 12.0,
                            ),
                            Text(
                              '${dateTimeFormat('HH:mm a', widget.record!.fromTime)} - ${dateTimeFormat('HH:mm a', widget.record!.toTime)}',
                              style: FlutterFlowTheme.of(context)
                                  .bodySmall
                                  .override(
                                    fontFamily: 'Urbanist',
                                    letterSpacing: 0.0,
                                    fontWeight: FontWeight.w500,
                                  ),
                            ),
                          ].divide(const SizedBox(width: 2.0)),
                        ),
                      ].divide(const SizedBox(height: 6.0)),
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(8.0),
                              child: SvgPicture.asset(
                                'assets/images/ImageIcon.svg',
                                fit: BoxFit.cover,
                              ),
                            ),
                            Text(
                              '12',
                              style: FlutterFlowTheme.of(context)
                                  .bodyMedium
                                  .override(
                                    fontFamily: 'Urbanist',
                                    color: const Color(0xFF00C07F),
                                    letterSpacing: 0.0,
                                    fontWeight: FontWeight.w500,
                                  ),
                            ),
                          ].divide(const SizedBox(width: 3.0)),
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(8.0),
                              child: SvgPicture.asset(
                                'assets/images/NoteIcon.svg',
                                fit: BoxFit.cover,
                              ),
                            ),
                            Text(
                              '1',
                              style: FlutterFlowTheme.of(context)
                                  .bodyMedium
                                  .override(
                                    fontFamily: 'Urbanist',
                                    color: const Color(0xFFEEB72E),
                                    letterSpacing: 0.0,
                                    fontWeight: FontWeight.w500,
                                  ),
                            ),
                          ].divide(const SizedBox(width: 3.0)),
                        ),
                      ].divide(const SizedBox(width: 14.0)),
                    ),
                  ].divide(const SizedBox(height: 12.0)),
                ),
              ),
            ].divide(const SizedBox(width: 10.0)),
          ),
        ),
      ),
    );
  }
}
