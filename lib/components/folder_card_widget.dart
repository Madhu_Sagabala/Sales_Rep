import 'package:flutter/material.dart';

import '/flutter_flow/flutter_flow_theme.dart';
import '/flutter_flow/flutter_flow_util.dart';
import 'folder_card_model.dart';

export 'folder_card_model.dart';

class FolderCardWidget extends StatefulWidget {
  const FolderCardWidget({super.key});

  @override
  State<FolderCardWidget> createState() => _FolderCardWidgetState();
}

class _FolderCardWidgetState extends State<FolderCardWidget> {
  late FolderCardModel _model;

  @override
  void setState(VoidCallback callback) {
    super.setState(callback);
    _model.onUpdate();
  }

  @override
  void initState() {
    super.initState();
    _model = createModel(context, () => FolderCardModel());
  }

  @override
  void dispose() {
    _model.maybeDispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(4.0),
          child: Container(
            width: 70.0,
            height: 70.0,
            decoration: BoxDecoration(
              // color: FlutterFlowTheme.of(context).secondaryBackground,
              borderRadius: BorderRadius.circular(4.0),
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(0.0),
              child: Image.network(
                'https://picsum.photos/seed/695/600',
                width: 300.0,
                height: 200.0,
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
        Text(
          'Office',
          style: FlutterFlowTheme.of(context).bodyMedium.override(
              fontFamily: 'Urbanist',
              fontSize: 10.0,
              letterSpacing: 0.0,
              fontWeight: FontWeight.w500,
              color: FlutterFlowTheme.of(context).text),
        ),
      ].divide(const SizedBox(height: 4.0)),
    );
  }
}
