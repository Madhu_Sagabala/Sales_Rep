import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sales_rep/Go_Route.dart';
import 'package:sales_rep/components/App_sub_title.dart';
import 'package:sales_rep/models/total_visits_response.dart';

import '/flutter_flow/flutter_flow_theme.dart';
import '/flutter_flow/flutter_flow_util.dart';

class VisitsCardWidget extends StatelessWidget {
  final Records visitsData;
  const VisitsCardWidget({super.key, required this.visitsData});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Colors.transparent,
      focusColor: Colors.transparent,
      hoverColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: () async {
        Get.toNamed(Routes.viewvisit);
      },
      child: Container(
        decoration: BoxDecoration(
          color: FlutterFlowTheme.of(context).visitCardColor,
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: 32.0,
                height: 32.0,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                ),
                child: Container(
                  clipBehavior: Clip.antiAlias,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                  ),
                  child: Image.network(
                    visitsData.images!.first,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Column(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            AppSubTitle(
                                text: DateFormat('y-MM-d')
                                    .format(visitsData.date!.toLocal()))
                          ],
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            AppSubTitle(text: visitsData.place.toString())
                          ],
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            const Icon(
                              Icons.access_time_filled_sharp,
                              color: Color(0xFF565656),
                              size: 12.0,
                            ),
                            AppSubTitle(
                                text:
                                    '${DateFormat('h:mm a').format(visitsData.fromTime!.toLocal())} - ${DateFormat('h:mm a').format(visitsData.toTime!.toLocal())}')
                          ].divide(const SizedBox(width: 2.0)),
                        ),
                      ].divide(const SizedBox(height: 6.0)),
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(8.0),
                              child: SvgPicture.asset(
                                'assets/images/ImageIcon.svg',
                                fit: BoxFit.cover,
                              ),
                            ),
                            Text(
                              visitsData.images!.length.toString(),
                              style: FlutterFlowTheme.of(context)
                                  .bodyMedium
                                  .override(
                                    fontFamily: 'Urbanist',
                                    color: const Color(0xFF00C07F),
                                    letterSpacing: 0.0,
                                    fontWeight: FontWeight.w500,
                                  ),
                            ),
                          ].divide(const SizedBox(width: 3.0)),
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(8.0),
                              child: SvgPicture.asset(
                                'assets/images/NoteIcon.svg',
                                fit: BoxFit.cover,
                              ),
                            ),
                            Text(
                              visitsData.notes!.length.toString(),
                              style: FlutterFlowTheme.of(context)
                                  .bodyMedium
                                  .override(
                                    fontFamily: 'Urbanist',
                                    color: const Color(0xFFEEB72E),
                                    letterSpacing: 0.0,
                                    fontWeight: FontWeight.w500,
                                  ),
                            ),
                          ].divide(const SizedBox(width: 3.0)),
                        ),
                      ].divide(const SizedBox(width: 14.0)),
                    ),
                  ].divide(const SizedBox(height: 12.0)),
                ),
              ),
            ].divide(const SizedBox(width: 10.0)),
          ),
        ),
      ),
    );
  }
}
