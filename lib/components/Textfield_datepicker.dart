import 'package:flutter/material.dart';
import 'package:sales_rep/flutter_flow/flutter_flow_theme.dart';
import 'package:sales_rep/flutter_flow/flutter_flow_util.dart';

// ProcurementController _procurementController = Get.put(ProcurementController());

class TextFeild_DatePicker extends StatefulWidget {
  final TextEditingController? dateController;
  final DateTime? firstDate;
  final DateTime? lastDate;
  DateTime? initialDate;
  final Function(String)? dateCallback;
  final String lable;
  final String? errorKey;
  final dynamic errors;
  TextFeild_DatePicker({
    super.key,
    required this.dateController,
    required this.lable,
    this.firstDate,
    this.lastDate,
    this.initialDate,
    this.errorKey,
    this.errors,
    this.dateCallback,
  });

  @override
  State<TextFeild_DatePicker> createState() => _TextFeild_DatePickerState();
}

class _TextFeild_DatePickerState extends State<TextFeild_DatePicker> {
  @override
  bool haserrorKey() {
    if (widget.errors != null) {
      return true;
    }
    return false;
  }

  String result() {
    if (haserrorKey()) {
      var data = widget.errors;
      return error(data, widget.errorKey);
    }
    return '';
  }

  String error(Map<String, dynamic> instance, String? errork) {
    for (var entries in instance.entries) {
      if (entries.key == errork) {
        return entries.value ?? '';
      }
    }
    return '';
  }

  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 6),
          child: Text(
            widget.lable,
            style: const TextStyle(fontWeight: FontWeight.w400, fontSize: 16),
          ),
        ),
        GestureDetector(
            onTap: () async {
              DateTime? pickedDate = await showDatePicker(
                context: context,
                initialDate: widget.initialDate ?? DateTime.now(),
                firstDate: widget.firstDate == null
                    ? DateTime(2000)
                    : widget.firstDate!,
                lastDate:
                    widget.lastDate == null ? DateTime(2050) : widget.lastDate!,
              );
              if (pickedDate != null) {
                DateTime currentTime = DateTime.now();
                pickedDate = DateTime(
                  pickedDate.year,
                  pickedDate.month,
                  pickedDate.day,
                  currentTime.hour,
                  currentTime.minute,
                  currentTime.second,
                );
                String formatedDate =
                    DateFormat("dd-MM-yyy").format(pickedDate.toLocal());
                String dateofProcurement =
                    DateFormat("yyy-MM-dd hh:mm:ss").format(pickedDate.toUtc());
              }
            },
            child: AbsorbPointer(
                child: Container(
              color: FlutterFlowTheme.of(context).secondaryBackground,
              height: 55,
              child: TextFormField(
                controller: widget.dateController,
                decoration: InputDecoration(
                  hintStyle: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      color: FlutterFlowTheme.of(context).secondaryText),
                  hintText: 'Select Date',
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8)), // Add border
                  suffixIcon: const Icon(Icons.calendar_today),
                ),
              ),
            ))),
        if (result().isNotEmpty && result().toString() != null)
          Padding(
            padding: const EdgeInsets.only(left: 13, top: 5.0),
            child: Text(
              result(),
              style: const TextStyle(
                  color: Color((0xffff3d34)),
                  fontSize: 12,
                  fontWeight: FontWeight.w400),
            ),
          ),
      ],
    );
  }
}
