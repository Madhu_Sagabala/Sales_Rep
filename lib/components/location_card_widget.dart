import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sales_rep/Go_Route.dart';
import 'package:sales_rep/models/total_location_response.dart';

import '/flutter_flow/flutter_flow_theme.dart';
import '/flutter_flow/flutter_flow_util.dart';

class LocationCardWidget extends StatefulWidget {
  final LocationRecord locationCard;
  const LocationCardWidget({super.key, required this.locationCard});

  @override
  State<LocationCardWidget> createState() => _LocationCardWidgetState();
}

class _LocationCardWidgetState extends State<LocationCardWidget> {
  @override
  void setState(VoidCallback callback) {
    super.setState(callback);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Get.toNamed(Routes.viewlocation);
      },
      child: Container(
          decoration: BoxDecoration(
            color: FlutterFlowTheme.of(context).locationCard,
            borderRadius: BorderRadius.circular(8.0),
          ),
          child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Row(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            width: 32.0,
                            height: 32.0,
                            decoration:
                                const BoxDecoration(shape: BoxShape.circle),
                            child: Container(
                                clipBehavior: Clip.antiAlias,
                                decoration:
                                    const BoxDecoration(shape: BoxShape.circle),
                                child: Image.network(
                                    widget.locationCard.avatar.toString(),
                                    fit: BoxFit.cover))),
                        Expanded(
                            child: Column(
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      Text(widget.locationCard.title.toString(),
                                          style: FlutterFlowTheme.of(context)
                                              .labelLarge
                                              .override(
                                                  fontFamily: 'Urbanist',
                                                  color: FlutterFlowTheme.of(
                                                          context)
                                                      .primaryText,
                                                  letterSpacing: 0.0))
                                    ],
                                  ),
                                  Row(
                                      mainAxisSize: MainAxisSize.max,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        const Icon(Icons.location_on_sharp,
                                            color: Color(0xFF7C7C7C),
                                            size: 12.0),
                                        Flexible(
                                            child: Text(
                                                widget
                                                    .locationCard.address
                                                    .toString(),
                                                style: FlutterFlowTheme.of(
                                                        context)
                                                    .bodyMedium
                                                    .override(
                                                        fontFamily: 'Urbanist',
                                                        color:
                                                            FlutterFlowTheme.of(
                                                                    context)
                                                                .secondaryText,
                                                        letterSpacing: 0.0)))
                                      ].divide(const SizedBox(width: 2.0))),
                                ].divide(const SizedBox(height: 6.0)))),
                        ClipOval(
                          child: Container(
                            width: 70.0,
                            height: 70.0,
                            decoration: BoxDecoration(
                                color: FlutterFlowTheme.of(context)
                                    .secondaryBackground,
                                shape: BoxShape.circle,
                                border:
                                    Border.all(color: const Color(0x49005BFE))),
                            child: Container(
                              clipBehavior: Clip.antiAlias,
                              decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                              ),
                              child: Image.asset(
                                'assets/images/MapImage.png',
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        ),
                      ].divide(const SizedBox(width: 12.0)))
                ],
              ))),
    );
  }
}
