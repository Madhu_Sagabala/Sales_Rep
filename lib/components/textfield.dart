import 'package:flutter/material.dart';
import 'package:sales_rep/core/form_error_helper.dart';
import 'package:sales_rep/flutter_flow/flutter_flow_theme.dart';

class TextFieldComponent extends StatelessWidget {
  final TextEditingController controller;
  final String? hintText;
  final String? errorKey;
  final Map<String, dynamic>? errors;
  final bool? isEnable;
  final TextInputType? inputType;
  final int? maxlines;
  const TextFieldComponent({
    super.key,
    required this.controller,
    this.hintText,
    this.errorKey,
    this.errors,
    this.inputType,
    this.maxlines,
    this.isEnable,
  });
  String? message() {
    return FormErrorHelper(errors: errors).message(errorKey!);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Column(
        children: [
          TextField(
            controller: controller,
            autofocus: true,
            obscureText: false,
            decoration: InputDecoration(
              labelStyle: FlutterFlowTheme.of(context).labelMedium.override(
                    fontFamily: 'Urbanist',
                    letterSpacing: 0.0,
                  ),
              hintText: hintText,
              hintStyle: FlutterFlowTheme.of(context).labelMedium.override(
                    fontFamily: 'Urbanist',
                    color: const Color(0xFFB6B6B6),
                    letterSpacing: 0.0,
                  ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: FlutterFlowTheme.of(context).alternate,
                  width: 1.0,
                ),
                borderRadius: BorderRadius.circular(8.0),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: FlutterFlowTheme.of(context).primary,
                  width: 1.0,
                ),
                borderRadius: BorderRadius.circular(8.0),
              ),
              errorBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: FlutterFlowTheme.of(context).error,
                  width: 1.0,
                ),
                borderRadius: BorderRadius.circular(8.0),
              ),
              focusedErrorBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: FlutterFlowTheme.of(context).error,
                  width: 1.0,
                ),
                borderRadius: BorderRadius.circular(8.0),
              ),
              filled: true,
              fillColor: FlutterFlowTheme.of(context)
                  .secondaryBackground
                  .withOpacity(0.4),
            ),
            style: FlutterFlowTheme.of(context).bodyMedium.override(
                  fontFamily: 'Urbanist',
                  letterSpacing: 0.0,
                ),
            keyboardType: inputType,
            enabled: isEnable,
            maxLines: maxlines,
            // validator:,
          ),
        ],
      ),
    );
  }
}
