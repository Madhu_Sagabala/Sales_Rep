import 'package:flutter/material.dart';
import 'package:sales_rep/flutter_flow/flutter_flow_theme.dart';

class AppSubTitle extends StatelessWidget {
  final String text;
  final TextAlign? textAlign;

   const AppSubTitle({super.key, required this.text, this.textAlign});
  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: FlutterFlowTheme.of(context).bodyMedium.override(
          fontFamily: 'Urbanist',
          letterSpacing: 0,
          fontWeight: FontWeight.w500,
          color: FlutterFlowTheme.of(context).text),
      textAlign: textAlign,
      overflow: TextOverflow.ellipsis,
      maxLines: 2,
    );
  }
}
