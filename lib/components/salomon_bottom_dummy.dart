import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:salomon_bottom_bar/salomon_bottom_bar.dart';

class SalomonBottomsheetDummy extends StatefulWidget {
  final List<Widget> pages;
  int currentIndex;
  SalomonBottomsheetDummy(
      {super.key, required this.pages, required this.currentIndex});
  @override
  State<SalomonBottomsheetDummy> createState() =>
      _SalomonBottomsheetDummyState();
}

class _SalomonBottomsheetDummyState extends State<SalomonBottomsheetDummy> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: widget.pages[widget.currentIndex],
      bottomNavigationBar: SalomonBottomBar(
        itemShape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        selectedColorOpacity: 1,
        selectedItemColor: const Color(0xff005BFE),
        backgroundColor: const Color(0xFFffffff),
        currentIndex: widget.currentIndex,
        onTap: (i) => setState(() => widget.currentIndex = i),
        items: [
          /// Home
          SalomonBottomBarItem(
            icon: widget.currentIndex == 0
                ? SvgPicture.asset(
                    'assets/images/HomeIocn.svg',
                    height: 24,
                    width: 24,
                    color: Colors.white,
                  )
                : SvgPicture.asset(
                    'assets/images/HomeIocn.svg',
                    height: 24,
                    width: 24,
                    color: const Color(0xff000000),
                  ),
            title: const Text("Home",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Urbanist',
                    fontSize: 12)),
          ),

          /// Likes
          SalomonBottomBarItem(
            icon: widget.currentIndex == 1
                ? SvgPicture.asset(
                    'assets/images/VisitIon.svg',
                    height: 24,
                    width: 24,
                    color: Colors.white,
                  )
                : SvgPicture.asset(
                    'assets/images/VisitIon.svg',
                    height: 24,
                    width: 24,
                    color: const Color(0xff000000),
                  ),
            title: const Text("Visits",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Urbanist',
                    fontSize: 12)),
          ),

          /// Search
          SalomonBottomBarItem(
            icon: widget.currentIndex == 2
                ? SvgPicture.asset(
                    'assets/images/LocationIcon.svg',
                    height: 24,
                    width: 24,
                    color: Colors.white,
                  )
                : SvgPicture.asset(
                    'assets/images/LocationIcon.svg',
                    height: 24,
                    width: 24,
                    color: const Color(0xff000000),
                  ),
            title: const Text("Location",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Urbanist',
                    fontSize: 12)),
          ),

          /// Profile
          SalomonBottomBarItem(
            icon: widget.currentIndex == 3
                ? SvgPicture.asset('assets/images/GalleryIcon.svg',
                    height: 24, width: 24, color: Colors.white)
                : SvgPicture.asset(
                    'assets/images/GalleryIcon.svg',
                    height: 24,
                    width: 24,
                    color: const Color(0xff000000),
                  ),
            title: const Text("Gallery",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Urbanist',
                    fontSize: 12)),
          ),
          SalomonBottomBarItem(
            icon: widget.currentIndex == 4
                ? SvgPicture.asset(
                    'assets/images/ProfileIcon.svg',
                    height: 24,
                    width: 24,
                    color: Colors.white,
                  )
                : SvgPicture.asset(
                    'assets/images/ProfileIcon.svg',
                    height: 24,
                    width: 24,
                    color: const Color(0xff000000),
                  ),
            title: const Text("Profile",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Urbanist',
                    fontSize: 12)),
          )
        ],
      ),
    );
  }
}
