import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sales_rep/components/App_sub_title.dart';

import '/flutter_flow/flutter_flow_icon_button.dart';
import '/flutter_flow/flutter_flow_theme.dart';
import '/flutter_flow/flutter_flow_util.dart';
import 'view_visit_controller.dart';

class ViewVisitWidget extends StatelessWidget {
  const ViewVisitWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final ViewVisitController controller = Get.put(ViewVisitController());

    return GestureDetector(
      child: Scaffold(
        backgroundColor: FlutterFlowTheme.of(context).primaryBackground,
        body: SafeArea(
          top: true,
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Padding(
                  padding: const EdgeInsetsDirectional.fromSTEB(16, 0, 16, 0),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      FlutterFlowIconButton(
                        borderColor: const Color(0xFFE9E9E9),
                        borderRadius: 20,
                        borderWidth: 1,
                        buttonSize: 40,
                        fillColor: Colors.white,
                        icon: const Icon(
                          Icons.arrow_back_sharp,
                          color: Colors.black,
                          size: 20,
                        ),
                        onPressed: () async {
                          Get.back();
                        },
                      ),
                      Text(
                        'View',
                        style:
                            FlutterFlowTheme.of(context).titleMedium.override(
                                  fontFamily: 'Urbanist',
                                  color: FlutterFlowTheme.of(context).text,
                                  letterSpacing: 0,
                                  fontWeight: FontWeight.w500,
                                ),
                      ),
                      FlutterFlowIconButton(
                        borderRadius: 8,
                        borderWidth: 0,
                        buttonSize: 40,
                        icon: Icon(
                          Icons.more_vert,
                          color: FlutterFlowTheme.of(context).primaryText,
                          size: 24,
                        ),
                        onPressed: () {
                          print('IconButton pressed ...');
                        },
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsetsDirectional.fromSTEB(0.0, 24.0, 0.0, 0.0),
                  child: Obx(() {
                    if (controller.singleview.value == null) {
                      if (controller.isLoading.value) {
                        return const Center(child: CircularProgressIndicator());
                      } else if (controller.hasError.value) {
                        return const Center(child: Text('No-Connection'));
                      } else {
                        return const Center(child: Text('No data available'));
                      }
                    } else {
                      final singleview = controller.singleview.value!;
                      return Column(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Row(
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Expanded(
                                child: Padding(
                                    padding:
                                        const EdgeInsetsDirectional.fromSTEB(
                                            16, 0, 16, 0),
                                    child: AppSubTitle(
                                        text: singleview.place.toString())),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsetsDirectional.fromSTEB(
                                16, 0, 16, 0),
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(8),
                                  child: SvgPicture.asset(
                                    'assets/images/calenderFill_Black.svg',
                                    fit: BoxFit.cover,
                                    color: FlutterFlowTheme.of(context)
                                        .primaryText,
                                  ),
                                ),
                                AppSubTitle(
                                    text: DateFormat('y-MM-d')
                                        .format(singleview.date!.toLocal()))
                              ].divide(const SizedBox(width: 8)),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsetsDirectional.fromSTEB(
                                16, 0, 16, 0),
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(8),
                                  child: SvgPicture.asset(
                                      'assets/images/LocationPinIcon.svg',
                                      fit: BoxFit.cover,
                                      color: FlutterFlowTheme.of(context)
                                          .primaryText),
                                ),
                                Expanded(
                                    child: AppSubTitle(
                                        text: singleview.place.toString())),
                              ].divide(const SizedBox(width: 8)),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsetsDirectional.fromSTEB(
                                16, 0, 16, 0),
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(8),
                                  child: SvgPicture.asset(
                                      'assets/images/clockIcon.svg',
                                      fit: BoxFit.cover,
                                      color: FlutterFlowTheme.of(context)
                                          .primaryText),
                                ),
                                Flexible(
                                    child: AppSubTitle(
                                  text: DateFormat('h:mm a')
                                      .format(singleview.fromTime!.toLocal()),
                                )),
                                const Flexible(child: AppSubTitle(text: '-')),
                                Flexible(
                                    child: AppSubTitle(
                                        text: DateFormat('h:mm a').format(
                                            singleview.toTime!.toLocal()))),
                              ].divide(const SizedBox(width: 8)),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsetsDirectional.fromSTEB(
                                16, 0, 16, 0),
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Expanded(
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      Row(
                                        mainAxisSize: MainAxisSize.max,
                                        children: [
                                          ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(8),
                                            child: SvgPicture.asset(
                                                'assets/images/pencil-01.svg',
                                                fit: BoxFit.cover,
                                                color:
                                                    FlutterFlowTheme.of(context)
                                                        .primaryText),
                                          ),
                                          Text(
                                            'Note',
                                            style: FlutterFlowTheme.of(context)
                                                .bodyMedium
                                                .override(
                                                  fontFamily: 'Urbanist',
                                                  color:
                                                      const Color(0xFFEEB72E),
                                                  fontSize: 16,
                                                  letterSpacing: 0,
                                                ),
                                          ),
                                        ].divide(const SizedBox(width: 8)),
                                      ),
                                      Row(
                                        mainAxisSize: MainAxisSize.max,
                                        children: [
                                          Expanded(
                                              child: AppSubTitle(
                                                  text: singleview.notes
                                                      .toString())),
                                        ],
                                      ),
                                    ].divide(const SizedBox(height: 12)),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Row(
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Expanded(
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  children: [
                                    Padding(
                                      padding:
                                          const EdgeInsetsDirectional.fromSTEB(
                                              16, 0, 16, 0),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.max,
                                        children: [
                                          ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(8),
                                            child: SvgPicture.asset(
                                              'assets/images/image-fill_1.svg',
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                          Text(
                                            'Images',
                                            style: FlutterFlowTheme.of(context)
                                                .bodyMedium
                                                .override(
                                                  fontFamily: 'Urbanist',
                                                  color:
                                                      const Color(0xFF00C07F),
                                                  fontSize: 16,
                                                  letterSpacing: 0,
                                                ),
                                          ),
                                        ].divide(const SizedBox(width: 8)),
                                      ),
                                    ),
                                    Row(
                                      mainAxisSize: MainAxisSize.max,
                                      children: [
                                        Expanded(
                                          child: Container(
                                            width: double.infinity,
                                            height: 425.0,
                                            child: CarouselSlider(
                                              items: singleview.images !=
                                                          null &&
                                                      singleview
                                                          .images!.isNotEmpty
                                                  ? singleview.images!
                                                      .map((imageUrl) =>
                                                          ClipRRect(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        8.0),
                                                            child:
                                                                Image.network(
                                                              imageUrl,
                                                              width: 300.0,
                                                              height: 200.0,
                                                              fit: BoxFit.cover,
                                                            ),
                                                          ))
                                                      .toList()
                                                  : [
                                                      const Text(
                                                          'No images available')
                                                    ],
                                              carouselController:
                                                  controller.carouselController,
                                              options: CarouselOptions(
                                                initialPage: 1,
                                                viewportFraction: 0.9,
                                                disableCenter: true,
                                                enlargeCenterPage: true,
                                                enlargeFactor: 0.2,
                                                enableInfiniteScroll: true,
                                                scrollDirection:
                                                    Axis.horizontal,
                                                autoPlay: false,
                                                onPageChanged: (index, _) {
                                                  controller
                                                      .carouselCurrentIndex
                                                      .value = index;
                                                },
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ].divide(const SizedBox(height: 12)),
                                ),
                              ),
                            ],
                          ),
                        ].divide(const SizedBox(height: 18)),
                      );
                    }
                  }),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
