import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sales_rep/backend/custom_response.dart';
import 'package:sales_rep/backend/services/view_visite_api.dart';
import 'package:sales_rep/models/single_view_visite_response.dart';

class ViewVisitController extends GetxController {
  /// State fields for stateful widgets in this page.

  final unfocusNode = FocusNode();
  var carouselController = CarouselController();
  var carouselCurrentIndex = 1.obs;
  var singleview = Rxn<Record>();
  var isLoading = true.obs;
  var hasError = false.obs;

  @override
  void onInit() {
    super.onInit();
    fetchviewvist();
  }

  @override
  void onClose() {
    unfocusNode.dispose();
    super.onClose();
  }

  void fetchviewvist() async {
    try {
      CustomResponse<SingleviewVisiteResponse> response =
          await SingleVisitViewApi().call();
      if (response.statusCode == 200) {
        singleview.value = response.data?.record;
      } else {
        isLoading.value = false;
        hasError.value = true;
      }
    } catch (e) {
      isLoading.value = false;
      hasError.value = true;
    }
  }
}
