import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sales_rep/core/form_error_helper.dart';
import 'package:sales_rep/flutter_flow/flutter_flow_theme.dart';

class FormTextField extends StatelessWidget {
  final TextEditingController controller;
  final bool isEnable;
  Function(String)? onChanged;
  final TextInputType? textInputType;
  int? maxLine = 1;
  int? minLine = 1;
  String? hintText;
  final Widget? suffixIcon;
  final bool obscureText;
  final bool isPasswordField;
  final String? errorKey;
  final List<TextInputFormatter>? inputFormatters;
  final dynamic errors;
  final Color? color;

  FormTextField({
    super.key,
    this.hintText = 'Select',
    this.isEnable = true,
    this.onChanged,
    this.color,
    this.errorKey,
    this.obscureText = false,
    this.suffixIcon,
    this.isPasswordField = false,
    this.errors,
    this.textInputType,
    required this.controller,
    this.maxLine,
    this.minLine,
    this.inputFormatters,
  });

  String? message() {
    return FormErrorHelper(errors: errors).message(errorKey!);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextFormField(
          onChanged: onChanged,
          minLines: minLine,
          maxLines: maxLine,
          controller: controller,
          inputFormatters: inputFormatters,
          autofocus: false,
          obscureText: obscureText,
          keyboardType: textInputType,
          decoration: InputDecoration(
              labelStyle: FlutterFlowTheme.of(context).labelMedium.override(
                    fontFamily: 'Urbanist',
                    color: color,
                    letterSpacing: 0.0,
                    fontWeight: FontWeight.w500,
                  ),
              enabled: isEnable,
              hintText: hintText,
              hintStyle: FlutterFlowTheme.of(context).labelMedium.override(
                    fontFamily: 'Urbanist',
                    color: FlutterFlowTheme.of(context).textfieldtext,
                    letterSpacing: 0.0,
                    fontWeight: FontWeight.w500,
                  ),
              filled: true,
              fillColor: FlutterFlowTheme.of(context).tertiary.withOpacity(0.4),
              isDense: true,
              errorText: message(),
              errorStyle: const TextStyle(color: Colors.red),
              border: OutlineInputBorder(
                borderSide: const BorderSide(color: Colors.grey, width: 1),
                borderRadius: BorderRadius.circular(8.0),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                    color:
                        FlutterFlowTheme.of(context).primary.withOpacity(0.9),
                    width: 1.0),
                borderRadius: BorderRadius.circular(8.0),
              ),
              errorBorder: OutlineInputBorder(
                borderSide: BorderSide(
                    color: FlutterFlowTheme.of(context).error, width: 1.0),
                borderRadius: BorderRadius.circular(8.0),
              ),
              focusedErrorBorder: OutlineInputBorder(
                borderSide: BorderSide(
                    color: FlutterFlowTheme.of(context).error, width: 1.0),
                borderRadius: BorderRadius.circular(8.0),
              ),
              suffixIcon: suffixIcon),
          style: FlutterFlowTheme.of(context).bodyMedium.override(
              fontFamily: 'Urbanist',
              color: FlutterFlowTheme.of(context).textfieldtext,
              letterSpacing: 0.0,
              fontWeight: FontWeight.w500),
        ),
      ],
    );
  }
}
