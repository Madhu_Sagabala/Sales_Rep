import 'dart:convert';

TotalVisitsResponse totalVisitsResponseFromJson(String str) =>
    TotalVisitsResponse.fromJson(json.decode(str));

String totalVisitsResponseToJson(TotalVisitsResponse data) =>
    json.encode(data.toJson());

class TotalVisitsResponse {
  List<Records>? record;
  Metadata? metadata;

  TotalVisitsResponse({
    this.record,
    this.metadata,
  });

  factory TotalVisitsResponse.fromJson(Map<String, dynamic> json) =>
      TotalVisitsResponse(
        record: json["record"] == null
            ? []
            : List<Records>.from(json["record"]!.map((x) => Records.fromJson(x))),
        metadata: json["metadata"] == null
            ? null
            : Metadata.fromJson(json["metadata"]),
      );

  Map<String, dynamic> toJson() => {
        "record": record == null
            ? []
            : List<dynamic>.from(record!.map((x) => x.toJson())),
        "metadata": metadata?.toJson(),
      };
}

class Metadata {
  String? id;
  bool? private;
  DateTime? createdAt;
  String? name;

  Metadata({
    this.id,
    this.private,
    this.createdAt,
    this.name,
  });

  factory Metadata.fromJson(Map<String, dynamic> json) => Metadata(
        id: json["id"],
        private: json["private"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "private": private,
        "createdAt": createdAt?.toIso8601String(),
        "name": name,
      };
}

class Records {
  String? id;
  DateTime? date;
  String? place;
  DateTime? fromTime;
  DateTime? toTime;
  List<String>? images;
  String? notes;

  Records({
    this.id,
    this.date,
    this.place,
    this.fromTime,
    this.toTime,
    this.images,
    this.notes,
  });

  factory Records.fromJson(Map<String, dynamic> json) => Records(
        id: json["_id"],
        date: json["date"] == null ? null : DateTime.parse(json["date"]),
        place: json["place"],
        fromTime: json["from_time"] == null
            ? null
            : DateTime.parse(json["from_time"]),
        toTime:
            json["to_time"] == null ? null : DateTime.parse(json["to_time"]),
        images: json["images"] == null
            ? []
            : List<String>.from(json["images"]!.map((x) => x)),
        notes: json["notes"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "date": date?.toIso8601String(),
        "place": place,
        "from_time": fromTime?.toIso8601String(),
        "to_time": toTime?.toIso8601String(),
        "images":
            images == null ? [] : List<dynamic>.from(images!.map((x) => x)),
        "notes": notes,
      };
}
