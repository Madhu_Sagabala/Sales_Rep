// To parse this JSON data, do
//
//     final VisitelistResponseDummy = VisitelistResponseDummyFromJson(jsonString);

import 'dart:convert';

VisitelistResponseDummy VisitelistResponseDummyFromJson(String str) =>
    VisitelistResponseDummy.fromJson(json.decode(str));

String VisitelistResponseDummyToJson(VisitelistResponseDummy data) =>
    json.encode(data.toJson());

class VisitelistResponseDummy {
  List<Record>? record;
  Metadata? metadata;

  VisitelistResponseDummy({
    this.record,
    this.metadata,
  });

  factory VisitelistResponseDummy.fromJson(Map<String, dynamic> json) =>
      VisitelistResponseDummy(
        record: json["record"] == null
            ? []
            : List<Record>.from(json["record"]!.map((x) => Record.fromJson(x))),
        metadata: json["metadata"] == null
            ? null
            : Metadata.fromJson(json["metadata"]),
      );

  Map<String, dynamic> toJson() => {
        "record": record == null
            ? []
            : List<dynamic>.from(record!.map((x) => x.toJson())),
        "metadata": metadata?.toJson(),
      };
}

class Metadata {
  String? id;
  bool? private;
  DateTime? createdAt;
  String? name;

  Metadata({
    this.id,
    this.private,
    this.createdAt,
    this.name,
  });

  factory Metadata.fromJson(Map<String, dynamic> json) => Metadata(
        id: json["id"],
        private: json["private"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "private": private,
        "createdAt": createdAt?.toIso8601String(),
        "name": name,
      };
}

class Record {
  String? id;
  DateTime? date;
  String? place;
  DateTime? fromTime;
  DateTime? toTime;
  List<String>? images;
  String? notes;
  DateTime? createdAt;
  DateTime? updatedAt;

  Record({
    this.id,
    this.date,
    this.place,
    this.fromTime,
    this.toTime,
    this.images,
    this.notes,
    this.createdAt,
    this.updatedAt,
  });

  factory Record.fromJson(Map<String, dynamic> json) => Record(
        id: json["_id"],
        date: json["date"] == null ? null : DateTime.parse(json["date"]),
        place: json["place"],
        fromTime: json["from_time"] == null
            ? null
            : DateTime.parse(json["from_time"]),
        toTime:
            json["to_time"] == null ? null : DateTime.parse(json["to_time"]),
        images: json["images"] == null
            ? []
            : List<String>.from(json["images"]!.map((x) => x)),
        notes: json["notes"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "date": date?.toIso8601String(),
        "place": place,
        "from_time": fromTime?.toIso8601String(),
        "to_time": toTime?.toIso8601String(),
        "images":
            images == null ? [] : List<dynamic>.from(images!.map((x) => x)),
        "notes": notes,
        "created_at": createdAt?.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
      };
}
