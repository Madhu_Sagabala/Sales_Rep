// To parse this JSON data, do
//
//     final totalLocationsResponse = totalLocationsResponseFromJson(jsonString);

import 'dart:convert';

TotalLocationsResponse totalLocationsResponseFromJson(String str) =>
    TotalLocationsResponse.fromJson(json.decode(str));

String totalLocationsResponseToJson(TotalLocationsResponse data) =>
    json.encode(data.toJson());

class TotalLocationsResponse {
  List<LocationRecord>? record;
  Metadata? metadata;

  TotalLocationsResponse({
    this.record,
    this.metadata,
  });

  factory TotalLocationsResponse.fromJson(Map<String, dynamic> json) =>
      TotalLocationsResponse(
        record: json["record"] == null
            ? []
            : List<LocationRecord>.from(
                json["record"]!.map((x) => LocationRecord.fromJson(x))),
        metadata: json["metadata"] == null
            ? null
            : Metadata.fromJson(json["metadata"]),
      );

  Map<String, dynamic> toJson() => {
        "record": record == null
            ? []
            : List<dynamic>.from(record!.map((x) => x.toJson())),
        "metadata": metadata?.toJson(),
      };
}

class Metadata {
  String? id;
  bool? private;
  DateTime? createdAt;
  String? name;

  Metadata({
    this.id,
    this.private,
    this.createdAt,
    this.name,
  });

  factory Metadata.fromJson(Map<String, dynamic> json) => Metadata(
        id: json["id"],
        private: json["private"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "private": private,
        "createdAt": createdAt?.toIso8601String(),
        "name": name,
      };
}

class LocationRecord {
  String? id;
  String? title;
  String? address;
  String? country;
  String? avatar;
  DateTime? createdAt;
  DateTime? updatedAt;

  LocationRecord({
    this.id,
    this.title,
    this.address,
    this.country,
    this.avatar,
    this.createdAt,
    this.updatedAt,
  });

  factory LocationRecord.fromJson(Map<String, dynamic> json) => LocationRecord(
        id: json["_id"],
        title: json["title"],
        address: json["address"],
        country: json["country"],
        avatar: json["avatar"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "title": title,
        "address": address,
        "country": country,
        "avatar": avatar,
        "created_at": createdAt?.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
      };
}
