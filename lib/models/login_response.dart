import 'dart:convert';

UserResponse userResponseFromJson(String str) {
  final jsonData = json.decode(str);
  return UserResponse.fromJson(jsonData);
}

String userResponseToJson(UserResponse data) => json.encode(data.toJson());

class UserResponse {
  final bool success;
  final String message;
  final Map<String, dynamic> errors;
  final Data? data;

  UserResponse({
    required this.success,
    required this.message,
    required this.errors,
    required this.data,
  });

  factory UserResponse.fromJson(Map<String, dynamic> json) {
    return UserResponse(
      success: json["success"] ?? false,
      message: json["message"] ?? "",
      errors: json['errors'] ?? {},
      data: json["data"] != null && json["data"] is Map
          ? Data.fromJson(json["data"])
          : null,
    );
  }

  Map<String, dynamic> toJson() => {
        "success": success,
        "message": message,
        "errors": errors,
        "data": data != null ? data!.toJson() : null,
      };
}

class Data {
  final UserDetails userDetails;
  final String accessToken;
  final String refreshToken;

  Data({
    required this.userDetails,
    required this.accessToken,
    required this.refreshToken,
  });

  factory Data.fromJson(Map<String, dynamic> json) {
    return Data(
      userDetails: UserDetails.fromJson(json["user_details"] ?? {}),
      accessToken: json["access_token"] ?? "",
      refreshToken: json["refresh_token"] ?? "",
    );
  }

  Map<String, dynamic> toJson() => {
        "user_details": userDetails.toJson(),
        "access_token": accessToken,
        "refresh_token": refreshToken,
      };
}

class UserDetails {
  final String id;
  final String fullName;
  final String userType;
  final String email;
  final String phone;
  final bool phoneVerified;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int v;
  final String password;
  final DateTime passwordExpiredAt;

  UserDetails({
    required this.id,
    required this.fullName,
    required this.userType,
    required this.email,
    required this.phone,
    required this.phoneVerified,
    required this.createdAt,
    required this.updatedAt,
    required this.v,
    required this.password,
    required this.passwordExpiredAt,
  });

  factory UserDetails.fromJson(Map<String, dynamic> json) {
    return UserDetails(
      id: json["_id"] ?? "",
      fullName: json["full_name"] ?? "",
      userType: json["user_type"] ?? "",
      email: json["email"] ?? "",
      phone: json["phone"] ?? "",
      phoneVerified: json["phone_verified"] ?? false,
      createdAt: json["createdAt"] != null
          ? DateTime.parse(json["createdAt"])
          : DateTime(0),
      updatedAt: json["updatedAt"] != null
          ? DateTime.parse(json["updatedAt"])
          : DateTime(0),
      v: json["__v"] ?? 0,
      password: json["password"] ?? "",
      passwordExpiredAt: json["password_expired_at"] != null
          ? DateTime.parse(json["password_expired_at"])
          : DateTime(0),
    );
  }

  Map<String, dynamic> toJson() => {
        "_id": id,
        "full_name": fullName,
        "user_type": userType,
        "email": email,
        "phone": phone,
        "phone_verified": phoneVerified,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "__v": v,
        "password": password,
        "password_expired_at": passwordExpiredAt.toIso8601String(),
      };
}

class Errors {
  String? email;
  String? password;

  Errors({
    this.email,
    this.password,
  });

  factory Errors.fromJson(Map<String, dynamic> json) => Errors(
        email: json["email"],
        password: json["password"],
      );

  Map<String, dynamic> toJson() => {
        "email": email,
        "password": password,
      };
}
