// To parse this JSON data, do
//
//     final DashboardstatsResponse = DashboardstatsResponseFromJson(jsonString);

import 'dart:convert';

DashboardstatsResponse DashboardstatsResponseFromJson(String str) =>
    DashboardstatsResponse.fromJson(json.decode(str));

String DashboardstatsResponseToJson(DashboardstatsResponse data) =>
    json.encode(data.toJson());

class DashboardstatsResponse {
  Record? record;
  Metadata? metadata;

  DashboardstatsResponse({
    this.record,
    this.metadata,
  });

  factory DashboardstatsResponse.fromJson(Map<String, dynamic> json) =>
      DashboardstatsResponse(
        record: json["record"] == null ? null : Record.fromJson(json["record"]),
        metadata: json["metadata"] == null
            ? null
            : Metadata.fromJson(json["metadata"]),
      );

  Map<String, dynamic> toJson() => {
        "record": record?.toJson(),
        "metadata": metadata?.toJson(),
      };
}

class Metadata {
  String? id;
  bool? private;
  DateTime? createdAt;
  String? name;

  Metadata({
    this.id,
    this.private,
    this.createdAt,
    this.name,
  });

  factory Metadata.fromJson(Map<String, dynamic> json) => Metadata(
        id: json["id"],
        private: json["private"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "private": private,
        "createdAt": createdAt?.toIso8601String(),
        "name": name,
      };
}

class Record {
  int? totalVisits;
  int? totalLocations;
  int? totalImages;

  Record({
    this.totalVisits,
    this.totalLocations,
    this.totalImages,
  });

  factory Record.fromJson(Map<String, dynamic> json) => Record(
        totalVisits: json["total_visits"],
        totalLocations: json["total_locations"],
        totalImages: json["total_images"],
      );

  Map<String, dynamic> toJson() => {
        "total_visits": totalVisits,
        "total_locations": totalLocations,
        "total_images": totalImages,
      };
}
