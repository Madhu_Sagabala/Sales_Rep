import 'package:get_storage/get_storage.dart';

class GetStoragereference {
  static const String accessToken = 'access-token';
  static const String visiteId = 'visiteId';

  static Future<void> setAccessToken(String value) async =>
      await GetStorage().write(accessToken, value);

  static String getAccessToken() => GetStorage().read(accessToken) ?? '';

  static Future<void> setvisiteId(String value) async =>
      await GetStorage().write(visiteId, value);

  static String getvisiteId() => GetStorage().read(visiteId) ?? '';

  static Future<void> remove(String key) async =>
      await GetStorage().remove(key);

  static Future<void> clear() async => await GetStorage().remove(accessToken);
}
