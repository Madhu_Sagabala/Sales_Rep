import 'package:sales_rep/flutter_flow/flutter_flow_util.dart';

export 'api_manager.dart';

// API VARIABLES

final String scheme = "https";
final String host = "dev-api.peepul.farm";
final String version = "v1.0";

String buildUrl(String path, {Map<String, dynamic>? queryParams}) {
  if (queryParams != null) {
    queryParams.removeWhere((key, value) => value == null || value == '');
  }
  path = path.replaceAll(RegExp('^/+'), '');
  var uri = Uri(
    scheme: '$scheme',
    host: '$host',
    path: '$version/$path',
    queryParameters: queryParams,
  );
  return uri.toString();
}

dynamic successMessage({required dynamic jsonBody}) => getJsonField(
      jsonBody,
      r'''$.message''',
    );
