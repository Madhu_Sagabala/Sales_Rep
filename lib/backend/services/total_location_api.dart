import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:sales_rep/backend/api_service.dart';
import 'package:sales_rep/backend/custom_response.dart';
import 'package:sales_rep/models/total_location_response.dart';

class TotalLocationsapi {
  Future<CustomResponse<TotalLocationsResponse>> call() async {
    ApiCallResponse response = await ApiManager.instance.makeApiCall(
      callName: 'Total_visits',
      apiUrl: 'https://api.jsonbin.io/v3/b/666a9185e41b4d34e4029dbe',
      callType: ApiCallType.GET,
      // headers: {'Authorization': SharedPreferenceHelper.getAccessToken()},
      params: {},
      bodyType: BodyType.JSON,
      returnBody: true,
      encodeBodyUtf8: false,
      decodeUtf8: false,
      cache: false,
    );

    return CustomResponse.completed(
        await compute(
            totalLocationsResponseFromJson, jsonEncode(response.jsonBody)),
        response.statusCode);
  }
}
