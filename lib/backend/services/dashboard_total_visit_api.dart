import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:sales_rep/backend/api_service.dart';
import 'package:sales_rep/backend/custom_response.dart';
import 'package:sales_rep/models/visite_list_model.dart';

class VisitslistApi {
  Future<CustomResponse<VisitelistResponseDummy>> call() async {
    ApiCallResponse response = await ApiManager.instance.makeApiCall(
      callName: 'dashboard_stats',
      apiUrl: 'api.jsonbin.io/v3/b/666a974ae41b4d34e4029fbf',
      callType: ApiCallType.GET,
      // headers: {'Authorization': SharedPreferenceHelper.getAccessToken()},
      params: {},
      bodyType: BodyType.JSON,
      returnBody: true,
      encodeBodyUtf8: false,
      decodeUtf8: false,
      cache: false,
    );

    return CustomResponse.completed(
        await compute(
            VisitelistResponseDummyFromJson, jsonEncode(response.jsonBody)),
        response.statusCode);
  }
}
