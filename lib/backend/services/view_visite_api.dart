import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:sales_rep/backend/api_service.dart';
import 'package:sales_rep/backend/custom_response.dart';
import 'package:sales_rep/models/single_view_visite_response.dart';

class SingleVisitViewApi {
  Future<CustomResponse<SingleviewVisiteResponse>> call() async {
    ApiCallResponse response = await ApiManager.instance.makeApiCall(
      callName: 'ViewVisit',
      apiUrl: 'api.jsonbin.io/v3/b/666a968cacd3cb34a856d35e',
      callType: ApiCallType.GET,
      // headers: {'Authorization': SharedPreferenceHelper.getAccessToken()},
      params: {},
      bodyType: BodyType.JSON,
      returnBody: true,
      encodeBodyUtf8: false,
      decodeUtf8: false,
      cache: false,
    );

    return CustomResponse.completed(
        await compute(
            singleviewVisiteResponseFromJson, jsonEncode(response.jsonBody)),
        response.statusCode);
  }
}
