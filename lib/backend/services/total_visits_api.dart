import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:sales_rep/backend/api_service.dart';
import 'package:sales_rep/backend/custom_response.dart';
import 'package:sales_rep/models/total_visits_response.dart';

class TotalVisitsapi {
  Future<CustomResponse<TotalVisitsResponse>> call() async {
    ApiCallResponse response = await ApiManager.instance.makeApiCall(
      callName: 'Total_visits',
      apiUrl: 'https://api.jsonbin.io/v3/b/666a8437ad19ca34f8785432',
      callType: ApiCallType.GET,
      // headers: {'Authorization': SharedPreferenceHelper.getAccessToken()},
      params: {},
      bodyType: BodyType.JSON,
      returnBody: true,
      encodeBodyUtf8: false,
      decodeUtf8: false,
      cache: false,
    );

    return CustomResponse.completed(
        await compute(
            totalVisitsResponseFromJson, jsonEncode(response.jsonBody)),
        response.statusCode);
  }
}
