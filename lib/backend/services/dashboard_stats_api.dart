import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:sales_rep/backend/api_service.dart';
import 'package:sales_rep/backend/custom_response.dart';

import '../../models/dashboard_stats_model.dart';

class TotalStatsApi {
  Future<CustomResponse<DashboardstatsResponse>> call() async {
    ApiCallResponse response = await ApiManager.instance.makeApiCall(
      callName: 'dashboard_stats',
      apiUrl: 'api.jsonbin.io/v3/b/666a9905acd3cb34a856d435',
      callType: ApiCallType.GET,
      // headers: {'Authorization': SharedPreferenceHelper.getAccessToken()},
      params: {},
      bodyType: BodyType.JSON,
      returnBody: true,
      encodeBodyUtf8: false,
      decodeUtf8: false,
      cache: false,
    );

    return CustomResponse.completed(
        await compute(
            DashboardstatsResponseFromJson, jsonEncode(response.jsonBody)),
        response.statusCode);
  }
}
