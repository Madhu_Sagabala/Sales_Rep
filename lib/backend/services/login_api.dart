import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:sales_rep/backend/api_service.dart';
import 'package:sales_rep/backend/custom_response.dart';
import 'package:sales_rep/models/login_response.dart';

class SignInApi {
  Future<CustomResponse<UserResponse>> call(
      {required String email, required String password}) async {
    final body = '''
{
  "email": "$email",
  "password": "$password"
}''';
    ApiCallResponse response = await ApiManager.instance.makeApiCall(
      callName: 'sign_in_api',
      apiUrl: buildUrl('/users/signin'),
      callType: ApiCallType.POST,
      headers: {},
      params: {},
      body: body,
      bodyType: BodyType.JSON,
      returnBody: true,
      encodeBodyUtf8: false,
      decodeUtf8: false,
      cache: false,
    );
    return CustomResponse.completed(
        await compute(userResponseFromJson, jsonEncode(response.jsonBody)),
        response.statusCode);
  }
}
