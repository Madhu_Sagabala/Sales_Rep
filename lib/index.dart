// Export pages
export '/pages/login/login_widget.dart' show LoginWidget;
export '/pages/dashboard/dashboard_widget.dart' show DashboardWidget;
export '/pages/totalvisits/totalvisits_widget.dart' show TotalvisitsWidget;
export '/pages/gallery/gallery_widget.dart' show GalleryWidget;
export '/pages/addvisit/addvisit_widget.dart' show AddvisitWidget;
export '/pages/splash_screen/splash_screen_widget.dart' show SplashScreenWidget;
export '/view_visit/view_visit_widget.dart' show ViewVisitWidget;
export '/pages/total_locations/total_locations_widget.dart'
    show TotalLocationsWidget;
export '/pages/add_location/add_location_widget.dart' show AddLocationWidget;
export '/view_location/view_location_widget.dart' show ViewLocationWidget;
export '/profile/profile_widget.dart' show ProfileWidget;
export '/folders/folders_widget.dart' show FoldersWidget;
export '/pages/gallery_folders/gallery_folders_widget.dart'
    show GalleryFoldersWidget;
