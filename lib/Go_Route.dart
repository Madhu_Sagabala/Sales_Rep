import 'package:get/get.dart';
import 'package:sales_rep/components/upload_image_widget.dart';
import 'package:sales_rep/index.dart';

class Routes {
  static const String splash = '/splash';
  static const String login = '/login';
  static const String dashboard = '/dashboard';
  static const String totalVisits = '/totalVisits';
  static const String totalLocatins = '/totalLocations';
  static const String gallery = '/gallery';
  static const String profile = '/profile';
  static const String uploadimage = '/uploadimage';
  static const String addLocation = '/Addlocation';
  static const String addVisit = '/Addvisit';
  static const String viewvisit = '/viewvisit';
  static const String viewlocation = '/viewlocation';
  static const String GalleryFolder = '/GalleryFolder';
  static const String SalomonBottomBar = '/SalomonBottomBar';
}

final getPages = [
  GetPage(
    name: Routes.splash,
    page: () => const SplashScreenWidget(),
  ),
  GetPage(
    name: Routes.login,
    page: () => const LoginWidget(),
  ),
  GetPage(
    name: Routes.dashboard,
    page: () => const DashboardWidget(),
  ),
  GetPage(
    name: Routes.totalVisits,
    page: () => const TotalvisitsWidget(),
  ),
  GetPage(
    name: Routes.totalLocatins,
    page: () =>  TotalLocationsWidget(),
  ),
  GetPage(
    name: Routes.gallery,
    page: () => const GalleryWidget(),
  ),
  GetPage(
    name: Routes.uploadimage,
    page: () => const UploadImageWidget(),
  ),
  GetPage(
    name: Routes.addVisit,
    page: () => const AddvisitWidget(),
  ),
  GetPage(
    name: Routes.addLocation,
    page: () => const AddLocationWidget(),
  ),
  GetPage(
    name: Routes.viewvisit,
    page: () => const ViewVisitWidget(),
  ),
  GetPage(
    name: Routes.viewlocation,
    page: () => const ViewLocationWidget(),
  ),
  GetPage(
    name: Routes.profile,
    page: () => const ProfileWidget(),
  ),
  GetPage(
    name: Routes.GalleryFolder,
    page: () => const GalleryFoldersWidget(),
  ),
  
];
